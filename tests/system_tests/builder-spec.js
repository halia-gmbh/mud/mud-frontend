const {protractor} = require("protractor");
const {by} = require("protractor");
const {element} = require("protractor");
const {browser} = require("protractor");

describe('builder test', function () {
  beforeAll(function() {
    browser.get("http://localhost:4200")
    element(by.id('mat-input-0')).sendKeys("i19053@hb.dhbw-stuttgart.de");
    element(by.id('mat-input-1')).sendKeys("12345");
    element(by.css(".mat-raised-button")).click();
    browser.ignoreSynchronization = true
    browser.wait(protractor.ExpectedConditions.urlIs("http://localhost:4200/overview"), 5000);
    element(by.css(".mat-raised-button")).click();
    element(by.id("dungeonNameInput")).sendKeys("Builder Tests");
    element(by.id("spielerAnzahlInput")).sendKeys("5");
    element(by.id("dungeonErstellenButton")).click();
    browser.wait(protractor.ExpectedConditions.urlContains('dungeon-builder'), 5000);
  });

  it('tests item template', function() {
    element(by.id("addItemTemplateButton")).click();
    element(by.id('itemName')).sendKeys('Testitem');
    element(by.id('itemWeight')).sendKeys('3');
    element(by.id('itemType')).click();
    element(by.id('accesoireOption')).click();
    element(by.id("createTemplate")).click();
    expect(element(by.css('td.cdk-column-name')).isPresent());
  });
});
