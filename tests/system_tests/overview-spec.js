const {protractor} = require("protractor");
const {by} = require("protractor");
const {element} = require("protractor");
const {browser} = require("protractor");

describe('overview test', function () {
  beforeEach(function() {
    browser.get("http://localhost:4200")
    element(by.id('mat-input-0')).sendKeys("i19053@hb.dhbw-stuttgart.de");
    element(by.id('mat-input-1')).sendKeys("12345");
    element(by.css(".mat-raised-button")).click();
    browser.wait(protractor.ExpectedConditions.urlIs("http://localhost:4200/overview"), 5000);
  });

  it('tests if navigation to builder works', function () {
    browser.ignoreSynchronization = true
    element(by.css(".mat-raised-button")).click();
    element(by.id("dungeonNameInput")).sendKeys("testDungeon");
    element(by.id("spielerAnzahlInput")).sendKeys("5");
    element(by.id("dungeonErstellenButton")).click();
    expect(protractor.ExpectedConditions.urlContains('dungeon-builder'));
  });
});
