const {protractor} = require("protractor");
const {by} = require("protractor");
const {element} = require("protractor");
const {browser} = require("protractor");

describe('login test', function () {
  it('tests user login', function () {
    browser.get("http://localhost:4200")
    element(by.id('mat-input-0')).sendKeys("i19053@hb.dhbw-stuttgart.de");
    element(by.id('mat-input-1')).sendKeys("12345");
    element(by.css(".mat-raised-button")).click();
    browser.wait(protractor.ExpectedConditions.urlIs("http://localhost:4200/overview"), 5000);
    expect(browser.driver.getCurrentUrl()).toEqual('http://localhost:4200/overview');
  });
});
