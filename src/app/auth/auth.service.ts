import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Account} from '../shared/interfaces/account';
import { map } from 'rxjs/operators';
import {api} from '../shared/api';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient, private router: Router) {
  }

  login(email: string, password: string): Promise<Account> {
    return this.httpClient.post<Account>(environment.baseUrl + api.login, {email, password})
      .pipe(
        map((userData: Account) => {
          sessionStorage.setItem('username', email);
          sessionStorage.setItem('userID', String(userData.id))
          let tokenStr = 'Bearer ' + userData.token;
          sessionStorage.setItem('token', tokenStr);
          return userData;
        })
      )
      .toPromise();
  }

  register(email: string, password: string): Promise<boolean> {
    return this.httpClient.post<boolean>(environment.baseUrl + api.register, {email, password})
      .toPromise();
  }

  logout(userID: string): Promise<boolean> {
    return this.httpClient.post<boolean>(environment.baseUrl + api.logout + userID, {userID})
      .toPromise().then(loggedOut =>{
      sessionStorage.clear();
      return loggedOut;
      });
  }

  public checkAuthState(route): Promise<boolean>  {
    return this.httpClient.get<boolean>(environment.baseUrl + api.checkAuthState)
      .toPromise().then(authState => {
        if (authState === false)
          this.router.navigate([''])
        if(route.url.length > 1){
          if(route.url[1].path === 'dungeon' || route.url[1].path === 'characterCreator') {
            console.log(route.url[2].path)
            if(sessionStorage.getItem('otherID') !== route.url[2].path) {
              this.router.navigate([''])
            }
          }
          if(route.url[0].path === 'dungeon-master' || route.url[0].path === 'dungeon-builder') {
            console.log(route.url[1].path)
            if(sessionStorage.getItem('otherID') !== route.url[1].path) {
              this.router.navigate([''])
            }
          }
          if(route.url[0].path === 'dungeon-player') {
            console.log(route.url[1].path)
            if(sessionStorage.getItem('otherID') !== route.url[2].path) {
              this.router.navigate([''])
            }
          }
        }
        sessionStorage.removeItem('otherID')
        return authState
      }).catch(() => this.router.navigate(['']))
  }
}
