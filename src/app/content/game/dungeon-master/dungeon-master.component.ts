import {ChangeDetectorRef, Component, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {CharacterClass} from '../../../shared/interfaces/character/character-class';
import {CharacterRace} from '../../../shared/interfaces/character/character-race';
import {Item} from '../../../shared/interfaces/room/item';
import {NPC, NPCAction} from '../../../shared/interfaces/room/npc';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DungeonStatusEnum, Game} from '../../../shared/interfaces/game';
import {MatrixRoom, Room} from '../../../shared/interfaces/room/room';
import {Direction} from '../../../shared/interfaces/room/direction';
import {ActivatedRoute, Router} from '@angular/router';
import {DungeonService} from '../../overview/dungeon.service';
import {DungeonBuilderService} from '../../builder/dungeon-builder.service';
import {Attribute} from '../../../shared/interfaces/character/attribute-set';
import {RoomEvent} from '../../../shared/interfaces/room/room-event';
import {InfoDialogComponent} from '../../../tools/dialogs/info-dialog/info-dialog.component';
import {ErrorDialogComponent} from '../../../tools/dialogs/error-dialog/error-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {environment} from '../../../../environments/environment';
import {Stomp} from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import {Character} from '../../../shared/interfaces/character/character';
import {DungeonMasterService} from '../dungeon-master.service';
import {MatTable} from '@angular/material/table';
import {routeConsts} from '../../../shared/routes';
import {Message, MessageType} from '../../../shared/interfaces/message';
import {DungeonPlayerService} from '../dungeon-player.service';
import {MatTabLabel} from '@angular/material/tabs';


@Component({
  selector: 'hd-dungeon-master',
  templateUrl: './dungeon-master.component.html',
  styleUrls: ['./dungeon-master.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class DungeonMasterComponent implements OnInit, OnDestroy {
  masterID: number;

  armour = 'ARMOUR';
  weapon = 'WEAPON';
  accessoire = 'ACCESSOIRE';

  expandedColumn: CharacterClass | CharacterRace | Item | NPC | null;
  itemTypes = [{'CONSUMABLE': 'Konsumierbar'}, {'ARMOUR': 'Rüstung'}, {'WEAPON': 'Waffe'}, {'ACCESSOIRE': 'Accesssoire'}];

  characterColumnsToDisplay = ['name', 'description', 'buttons'];
  itemColumnsToDisplay = ['name', 'weight', 'itemType', 'buttons'];
  eventColumnsToDisplay = ['name', 'buttons'];
  npcColumnsToDisplay = ['name', 'buttons'];
  playerColumnsToDisplay = ['name', 'room', 'buttons'];

  itemTemplateForm: FormGroup;
  itemAttributeTemplateForm: FormGroup;
  textEventTemplateForm: FormGroup;
  itemEventTemplateForm: FormGroup;
  npcTemplateForm: FormGroup;
  npcTemplateActionForm: FormGroup;

  itemForm: FormGroup;
  itemAttributeForm: FormGroup;
  textEventForm: FormGroup;
  itemEventForm: FormGroup;
  npcForm: FormGroup;
  npcActionForm: FormGroup;

  createRoomForm: FormGroup;
  editRoomForm: FormGroup;
  togglePathChange: boolean = false;

  editObjectForm: FormGroup;

  characterAttributeForm: FormGroup;

  characterEquipmentForm: FormGroup;
  characterEquipmentAttributeForm: FormGroup;

  characterItemForm: FormGroup;
  characterItemAttributeForm: FormGroup;
  addCharacterItemForm: FormGroup;

  equipArmourForm: FormGroup;
  equipWeaponForm: FormGroup;
  equipAccessoireForm: FormGroup;

  changeMasterForm: FormGroup;

  private stompClient;
  chatForm: FormGroup;
  playerMessages: string[] = [];
  commandType: string = 'Spieler';
  types: string[] = ['Spieler', 'Raum', 'Dungeon'];

  selectedCharacter: Character;
  selectedCharacterIndex: number;

  gameID: number;
  game: Game = {
    id: null,
    name: '',
    builder: 0,
    dungeonMaster: 0,
    maxActivePlayers: 0,
    dungeonStatus: DungeonStatusEnum.WIP,
    classes: [],
    races: [],
    running: false,
    dungeon: {
      startRoom: null,
      itemTemplates: [],
      npcTemplates: [],
      eventTemplates: [],
      rooms: [],
      activeCharacters: []
    },
    password: null
  };
  selectedRoom: MatrixRoom;
  selectedRoomPosition = [];
  rooms: (MatrixRoom | Direction)[][] = [
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, Direction.START, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
  ];

  constructor(private router: Router,
              private dungeonMasterService: DungeonMasterService,
              private route: ActivatedRoute,
              private dungeonService: DungeonService,
              private dungeonBuilderService: DungeonBuilderService,
              private dungeonPlayerService: DungeonPlayerService,
              private dialog: MatDialog,
              private changeDetectorRefs: ChangeDetectorRef){

  }

  activeCharacterSource: any[];
  @ViewChild('charTable') table: MatTable<any>;

  correctlyEnded : boolean = false;

  resetWatch(gameID) {
    console.log(this.gameID)
    const _this = this;
    this.dungeonMasterService.resetDeathWatch(gameID).toPromise().then();
    window.setTimeout(function() {
      _this.resetWatch(gameID);
    }, 30000);
  }

  ngOnInit(): void {

    this.gameID = +this.route.snapshot.paramMap.get('gameID');
    this.masterID = +sessionStorage.getItem('userID');

    this.dungeonMasterService.setDeathWatch(this.gameID).toPromise().then();

    this.changeMasterForm = new FormGroup({
      character: new FormControl('', Validators.required)
    })

    this.createRoomForm = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required)
    });
    this.editRoomForm = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required)
    });

    this.itemTemplateForm = new FormGroup({
      name: new FormControl('', Validators.required),
      weight: new FormControl('', Validators.required),
      itemType: new FormControl('', Validators.required)
    });
    this.itemAttributeTemplateForm = new FormGroup({
      name: new FormControl('', Validators.required),
      value: new FormControl('', Validators.required)
    });
    this.npcTemplateForm = new FormGroup({
      name: new FormControl('', Validators.required)
    });
    this.npcTemplateActionForm = new FormGroup({
      name: new FormControl('', Validators.required),
      response: new FormControl('', Validators.required)
    });
    this.textEventTemplateForm = new FormGroup({
      name: new FormControl('', Validators.required),
      textResponse: new FormControl('', Validators.required)
    });
    this.itemEventTemplateForm = new FormGroup({
      name: new FormControl('', Validators.required),
      itemResponse: new FormControl('', Validators.required)
    });

    this.itemForm = new FormGroup({
      name: new FormControl('', Validators.required),
      weight: new FormControl('', Validators.required),
      itemType: new FormControl('', Validators.required)
    });
    this.itemAttributeForm = new FormGroup({
      name: new FormControl('', Validators.required),
      value: new FormControl('', Validators.required)
    });
    this.npcForm = new FormGroup({
      name: new FormControl('', Validators.required)
    });
    this.npcActionForm = new FormGroup({
      name: new FormControl('', Validators.required),
      response: new FormControl('', Validators.required)
    });
    this.textEventForm = new FormGroup({
      name: new FormControl('', Validators.required),
      textResponse: new FormControl('', Validators.required)
    });
    this.itemEventForm = new FormGroup({
      name: new FormControl('', Validators.required),
      itemResponse: new FormControl('', Validators.required)
    });

    this.characterAttributeForm = new FormGroup({
      name: new FormControl('', Validators.required),
      value: new FormControl('', Validators.required)
    });
    this.characterEquipmentForm = new FormGroup({
      name: new FormControl('', Validators.required),
      weight: new FormControl('', Validators.required),
      itemType: new FormControl('', Validators.required)
    });
    this.characterEquipmentAttributeForm = new FormGroup({
      name: new FormControl('', Validators.required),
      value: new FormControl('', Validators.required)
    });
    this.characterItemForm = new FormGroup({
      name: new FormControl('', Validators.required),
      weight: new FormControl('', Validators.required),
      itemType: new FormControl('', Validators.required)
    });
    this.characterItemAttributeForm = new FormGroup({
      name: new FormControl('', Validators.required),
      value: new FormControl('', Validators.required)
    });
    this.addCharacterItemForm = new FormGroup({
      item: new FormControl('', Validators.required)
    });
    this.equipArmourForm = new FormGroup({
      item: new FormControl('', Validators.required)
    });
    this.equipWeaponForm = new FormGroup({
      item: new FormControl('', Validators.required)
    });
    this.equipAccessoireForm = new FormGroup({
      item: new FormControl('', Validators.required)
    });

    this.chatForm = new FormGroup({
      message: new FormControl('', Validators.required)
    });

    this.dungeonService.getGame(this.gameID).toPromise().then((game: Game) => {
      this.game = game;
      if (game.dungeon.rooms.length != 0) {
        this.dungeonBuilderService.getRoomMatrix(this.gameID).toPromise().then(matrix => {
          this.rooms = matrix;
        });
      }
      this.activeCharacterSource = game.dungeon.activeCharacters;
    });

    const _this = this;
/*
    window.addEventListener("beforeunload", function (e) {
      if(!_this.correctlyEnded && !_this.onBeforeUnLoadEvent) {
        _this.onBeforeUnLoadEvent = true;
        _this.dungeonMasterService.setDeathWatch(_this.gameID).toPromise().then();
      }
    });

    window.addEventListener("unload", function (e) {
      if(!_this.correctlyEnded && !_this.onBeforeUnLoadEvent) {
        _this.onBeforeUnLoadEvent = true;
        _this.dungeonMasterService.setDeathWatch(_this.gameID).toPromise().then();
      }
    });
 */
    this.connect();
    this.resetWatch(this.gameID);
  }

 // onBeforeUnLoadEvent = false

  /*
  @HostListener('window:beforeunload')
  befunloadHandler(event) {
    if(!this.correctlyEnded && !this.onBeforeUnLoadEvent) {
      this.onBeforeUnLoadEvent = true;
      this.dungeonMasterService.setDeathWatch(this.gameID).toPromise().then();
    }
  }

  @HostListener('window:unload')
  unloadHandler(event) {
    if(!this.correctlyEnded && !this.onBeforeUnLoadEvent) {
      this.onBeforeUnLoadEvent = true;
      this.dungeonMasterService.setDeathWatch(this.gameID).toPromise().then();
    }
  }

  */

  ngOnDestroy() {
    this.disconnectFromAll();
    //if(!this.correctlyEnded && !this.onBeforeUnLoadEvent) {
     // this.onBeforeUnLoadEvent = true;
    //  this.dungeonMasterService.setDeathWatch(this.gameID).toPromise().then();
   // }
    console.log("failed")
  }

  // GENERAL
  endDungeon() {
    this.dungeonMasterService.endDungeon(this.gameID).toPromise()
      .then(ans => {
        if (ans) {
          this.correctlyEnded = true;
          this.router.navigate([routeConsts.overviewNav]);
        } else {
          this.dialog.open(InfoDialogComponent, {
            data: 'Ein Fehler ist aufgetreten.'
          });
        }
      })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  changeMaster() {
    this.dungeonMasterService.changeDungeonMaster(this.gameID, this.changeMasterForm.value.character)
      .toPromise().then(ans => {
        if (ans == true) {
          this.router.navigate([routeConsts.dungeonOverViewNav+this.gameID])
        }
        else {
          this.dialog.open(InfoDialogComponent, {
            data: 'Der Dungeon Master konnte nicht gewechselt werden. Versuchen Sie es erneut.'
          })
        }
    }).catch(() => {
      this.dialog.open(InfoDialogComponent, {
        data: 'Ein Fehler ist aufgetreten.'
      })
    })
  }


  // CHAT
  sendMessage() {
    const message: Message = {
      senderName: String(this.masterID),
      receiverName: '',
      content: this.chatForm.value.message,
      type: null,
      roomID: null,
      gameID: this.gameID
    };

    switch (this.commandType) {
      case 'Spieler':
        if (this.selectedCharacter) {
          message.receiverName = this.selectedCharacter.name;
          message.type = MessageType.DmToPlayer;
          this.dungeonService.chat(message).toPromise().then(() => this.chatForm.reset());
          this.playerMessages.push('An Spieler ' + this.selectedCharacter.name + ': ' + message.content);
        } else {
          this.dialog.open(InfoDialogComponent, {
            data: 'Es ist kein Spieler ausgewählt an den eine Nachricht geschickt werden kann!'
          });
        }
        break;
      case 'Raum':
        if (this.selectedRoom && this.selectedRoom.room) {
          message.roomID = this.selectedRoom.room.id;
          message.type = MessageType.DmTalk;
          this.dungeonService.chat(message).toPromise().then(() => this.chatForm.reset());
          this.playerMessages.push('An Raum  ' + this.selectedRoom.room.name + ': ' + message.content);
        } else {
          this.dialog.open(InfoDialogComponent, {
            data: 'Es ist kein Raum ausgewählt an den eine Nachricht geschickt werden kann!'
          });
        }
        break;
      case 'Dungeon':
        message.type = MessageType.DmShout;
        this.dungeonService.chat(message).toPromise().then(() => this.chatForm.reset());
        this.playerMessages.push('An Dungeon: ' + message.content);
        break;
    }
  }

  // PLAYER
  selectPlayer(character: Character, index: number) {
    this.selectedCharacter = character;
    this.selectedCharacterIndex = index;
  }

  moveToRoom(character: Character) {
    if (this.selectedRoom && this.selectedRoom.room) {
      this.dungeonMasterService.placePlayer(this.gameID, character.id, this.selectedRoom.room.id).toPromise().then(c => {
        character.location = c.location;
      });
    } else {
      this.dialog.open(InfoDialogComponent, {
        data: 'Es is kein Raum selektiert in den der Spieler gesetzt werden kann!'
      });
    }
  }

  // CHARACTER-OPTIONS
  // CHARACTER
  setEditCharacterAttributeValues(attribute: Attribute) {
    this.characterAttributeForm.controls.name.setValue(attribute.name);
    this.characterAttributeForm.controls.value.setValue(attribute.value);
  }

  editCharacterAttribute(attIndex: number) {
    const resultForm = this.characterAttributeForm.value;

    this.dungeonMasterService.setAttribute(this.gameID, this.selectedCharacter.id, attIndex, resultForm.name, resultForm.value)
      .toPromise()
      .then(character => {
        if (character) {
          this.selectedCharacter = character;
          this.game.dungeon.activeCharacters[this.selectedCharacterIndex] = character;
          this.characterAttributeForm.reset();
        } else {
          this.dialog.open(InfoDialogComponent, {
            data: 'Ein Fehler ist aufgetreten.'
          });
        }
      })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  deleteCharacterAttribute(attribute: Attribute, attIndex: number) {
    this.dungeonMasterService.deleteAttribute(this.gameID, this.selectedCharacter.id, attribute.name)
      .toPromise()
      .then(ans => {
        if (ans) {
          this.game.dungeon.activeCharacters[this.selectedCharacterIndex].attributes.attributes.splice(attIndex, 1);
        } else {
          this.dialog.open(InfoDialogComponent, {
            data: 'Ein Fehler ist aufgetreten.'
          });
        }
      })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  addCharacterAttribute() {
    const resultForm = this.characterAttributeForm.value;

    this.dungeonMasterService.setAttribute(this.gameID, this.selectedCharacter.id, -1, resultForm.name, resultForm.value)
      .toPromise()
      .then(character => {
        if (character) {
          this.game.dungeon.activeCharacters[this.selectedCharacterIndex] = character
          this.characterAttributeForm.reset();
        } else {
          this.dialog.open(InfoDialogComponent, {
            data: 'Ein Fehler ist aufgetreten.'
          });
        }
      })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  // EQUIPMENT
  setEditCharacterEquipment(item: Item) {
    this.characterEquipmentForm.controls.name.setValue(item.name);
    this.characterEquipmentForm.controls.weight.setValue(item.weight);
  }

  setEditCharacterEquipmentAttributeValues(attribute: Attribute) {
    this.characterEquipmentAttributeForm.controls.name.setValue(attribute.name);
    this.characterEquipmentAttributeForm.controls.value.setValue(attribute.value);
  }

  deleteCharacterEquipment(itemID: number) {
    this.dungeonMasterService.deleteItem(this.gameID, this.selectedCharacter.id, itemID).toPromise().then(character => {
      if (character) {
        this.selectedCharacter = character;
        this.game.dungeon.activeCharacters[this.selectedCharacterIndex] = character;
      } else {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      }
    })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  addCharacterEquipmentAttribute(item: Item) {
    const resultForm = this.characterEquipmentAttributeForm.value;
    item.attributes.attributes.push({name: resultForm.name, value: resultForm.value});
    this.dungeonMasterService.setItem(this.gameID, this.selectedCharacter.id, item).toPromise().then(character => {
      if (character) {
        this.selectedCharacter = character;
        this.game.dungeon.activeCharacters[this.selectedCharacterIndex] = character;
        this.characterEquipmentAttributeForm.reset();
      } else {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      }
    })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  deleteCharacterEquipmentAttribute(item: Item, attIndex: number) {
    item.attributes.attributes.splice(attIndex, 1);
    this.dungeonMasterService.setItem(this.gameID, this.selectedCharacter.id, item).toPromise().then(character => {
      if (character) {
        this.selectedCharacter = character;
        this.game.dungeon.activeCharacters[this.selectedCharacterIndex] = character;
        this.characterEquipmentForm.reset();
      } else {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      }
    })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  editCharacterEquipmentAttribute(item: Item, attIndex: number) {
    const resultForm = this.characterEquipmentAttributeForm.value;
    item.attributes.attributes[attIndex].name = resultForm.name;
    item.attributes.attributes[attIndex].value = resultForm.value;
    this.dungeonMasterService.setItem(this.gameID, this.selectedCharacter.id, item)
      .toPromise()
      .then(character => {
        if (character) {
          this.selectedCharacter = character;
          this.game.dungeon.activeCharacters[this.selectedCharacterIndex] = character;
          this.characterEquipmentAttributeForm.reset();
        } else {
          this.dialog.open(InfoDialogComponent, {
            data: 'Ein Fehler ist aufgetreten.'
          });
        }
      })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  equipPlayerArmour() {
    const resultForm = this.equipArmourForm.value;

    this.dungeonPlayerService.useItem(this.gameID, this.selectedCharacter.id, resultForm.item.id).toPromise()
      .then(character => {
        this.selectedCharacter = character;
        this.game.dungeon.activeCharacters[this.selectedCharacterIndex] = character;
        this.equipArmourForm.reset();
      });
  }

  equipPlayerWeapon() {
    const resultForm = this.equipWeaponForm.value;

    this.dungeonPlayerService.useItem(this.gameID, this.selectedCharacter.id, resultForm.item.id).toPromise()
      .then(character => {
        this.selectedCharacter = character;
        this.game.dungeon.activeCharacters[this.selectedCharacterIndex] = character;
        this.equipWeaponForm.reset();
      });
  }

  equipPlayerAccessoire() {
    const resultForm = this.equipAccessoireForm.value;

    this.dungeonPlayerService.useItem(this.gameID, this.selectedCharacter.id, resultForm.item.id).toPromise()
      .then(character => {
        this.selectedCharacter = character;
        this.game.dungeon.activeCharacters[this.selectedCharacterIndex] = character;
        this.equipAccessoireForm.reset();
      });
  }

  unEquipItem(itemID: number) {
    this.dungeonPlayerService.unEquipItem(this.gameID, this.selectedCharacter.id, itemID)
      .toPromise()
      .then(character => {
        this.selectedCharacter = character;
        this.game.dungeon.activeCharacters[this.selectedCharacterIndex] = character;
      });
  }

  filterPlayerEquipment(items: Item[], itemType: string) {
    return items.filter(i => i.itemType.toString() === itemType);
  }

  // INVENTORY
  setEditCharacterItemValues(item: Item) {
    this.characterItemForm.controls.name.setValue(item.name);
    this.characterItemForm.controls.weight.setValue(item.weight);
  }

  setEditCharacterItemAttributeValues(attribute: Attribute) {
    this.characterItemAttributeForm.controls.name.setValue(attribute.name);
    this.characterItemAttributeForm.controls.value.setValue(attribute.value);
  }

  addCharacterItem() {
    const item: Item = {
      ...this.addCharacterItemForm.value.item,
      id: -1
    };
    this.dungeonMasterService.setItem(this.gameID, this.selectedCharacter.id, item).toPromise().then(character => {
      if (character) {
        this.selectedCharacter = character;
        this.game.dungeon.activeCharacters[this.selectedCharacterIndex] = character;
        this.addCharacterItemForm.reset();
      } else {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      }
    })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  editCharacterItem(item: Item) {
    const resultForm = this.characterItemForm.value;
    item.name = resultForm.name;
    item.weight = resultForm.weight;

    this.dungeonMasterService.setItem(this.gameID, this.selectedCharacter.id, item).toPromise().then(character => {
      if (character) {
        this.selectedCharacter = character;
        this.game.dungeon.activeCharacters[this.selectedCharacterIndex] = character;
        this.characterItemForm.reset();
      } else {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      }
    })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  deleteCharacterItem(itemID: number) {
    this.dungeonMasterService.deleteItem(this.gameID, this.selectedCharacter.id, itemID).toPromise().then(character => {
      if (character) {
        this.selectedCharacter = character;
        this.game.dungeon.activeCharacters[this.selectedCharacterIndex] = character;
      } else {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      }
    })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  addCharacterItemAttribute(item: Item) {
    const resultForm = this.characterItemAttributeForm.value;

    item.attributes.attributes.push({name: resultForm.name, value: resultForm.value});

    this.dungeonMasterService.setItem(this.gameID, this.selectedCharacter.id, item).toPromise().then(ans => {
      if (ans) {
        this.characterItemAttributeForm.reset();
      } else {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      }
    })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  editCharacterItemAttribute(attribute: Attribute, item: Item) {
    const resultForm = this.characterItemAttributeForm.value;
    attribute.name = resultForm.name;
    attribute.value = resultForm.value;

    this.dungeonMasterService.setItem(this.gameID, this.selectedCharacter.id, item).toPromise().then(ans => {
      if (ans) {
        this.characterItemAttributeForm.reset();
      } else {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      }
    })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  deleteCharacterItemAttribute(item: Item, attIndex: number) {
    item.attributes.attributes.splice(attIndex, 1);

    this.dungeonMasterService.setItem(this.gameID, this.selectedCharacter.id, item)
      .toPromise()
      .then()
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  useConsumable(itemID: number) {
    this.dungeonPlayerService.useItem(this.gameID, this.selectedCharacter.id, itemID).toPromise()
      .then(character => {
        this.selectedCharacter = character;
      });
  }


  // ROOM-OBJECTS
  // ITEM
  setEditItemAttributeValues(attribute: Attribute) {
    this.itemAttributeForm.controls.name.setValue(attribute.name);
    this.itemAttributeForm.controls.value.setValue(attribute.value);
  }

  setEditItemValues(item: Item) {
    this.itemForm.controls.name.setValue(item.name);
    this.itemForm.controls.weight.setValue(item.weight);
  }

  addItem() {
    const resultForm = this.itemForm.value;

    const item: Item = {
      id: -1,
      name: resultForm.name,
      weight: resultForm.weight,
      attributes: {attributes: []},
      itemType: resultForm.itemType
    };
    this.dungeonBuilderService.addItemToRoom(this.gameID, this.selectedRoom.room.id, item).toPromise().then(game => {
      if (game) {

        this.game = game;
        this.itemForm.reset();
        this.dungeonBuilderService.getRoomMatrix(this.gameID).toPromise().then(matrix => {
          if (matrix) {

            this.rooms = matrix;
            this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
          } else {
            this.dialog.open(InfoDialogComponent, {
              data: 'Ein Fehler ist aufgetreten.'
            });
          }
        })
          .catch(() => {
            this.dialog.open(InfoDialogComponent, {
              data: 'Ein Fehler ist aufgetreten.'
            });
          });
      } else {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      }
    })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  editItem(item: Item) {
    const resultForm = this.itemForm.value;
    item.name = resultForm.name;
    item.weight = resultForm.weight;

    this.dungeonBuilderService.addItemToRoom(this.gameID, this.selectedRoom.room.id, item).toPromise().then(game => {
      this.game = game;
      this.itemForm.reset();
    });
  }

  addItemAttribute(item: Item) {
    const resultForm = this.itemAttributeForm.value;

    item.attributes.attributes.push({'name': resultForm.name, 'value': resultForm.value});

    this.dungeonBuilderService.addItemToRoom(this.gameID, this.selectedRoom.room.id, item).toPromise().then(game => {
      if (game) {

        this.game = game;
        this.itemAttributeForm.reset();
      } else {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      }
    })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  editItemAttribute(attribute: Attribute, item: Item) {
    const resultForm = this.itemAttributeForm.value;
    attribute.name = resultForm.name;
    attribute.value = resultForm.value;

    this.dungeonBuilderService.addItemToRoom(this.gameID, this.selectedRoom.room.id, item).toPromise().then(game => {
      if (game) {

        this.game = game;
        this.itemAttributeForm.reset();
      } else {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      }
    })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  deleteItem(itemID: number) {
    this.dungeonBuilderService.removeItemFromRoom(this.gameID, this.selectedRoom.room.id, itemID)
      .toPromise().then(game => {
      if (game) {

        this.game = game;
        this.dungeonBuilderService.getRoomMatrix(this.gameID).toPromise().then(matrix => {
          if (matrix) {

            this.rooms = matrix;
            this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
          } else {
            this.dialog.open(InfoDialogComponent, {
              data: 'Ein Fehler ist aufgetreten.'
            });
          }
        })
          .catch(() => {
            this.dialog.open(InfoDialogComponent, {
              data: 'Ein Fehler ist aufgetreten.'
            });
          });
      } else {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      }
    })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  deleteItemAttribute(item: Item, attributeIndex: number) {
    item.attributes.attributes.splice(attributeIndex, 1);

    this.dungeonBuilderService.addItemToRoom(this.gameID, this.selectedRoom.room.id, item).toPromise().then(game => {
      if (game) {
        this.game = game;
      } else {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      }
    })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  // ROOMEVENT
  setEditTextEventValues(roomEvent: RoomEvent) {
    this.textEventForm.controls.name.setValue(roomEvent.name);
    this.textEventForm.controls.textResponse.setValue(roomEvent.textResponse);
  }

  setEditItemEventValues(roomEvent: RoomEvent) {
    this.itemEventForm.controls.name.setValue(roomEvent.name);
    this.itemEventForm.controls.itemResponse.setValue(roomEvent.itemResponse);
  }

  addTextEvent() {
    const resultForm = this.textEventForm.value;

    const roomEvent: RoomEvent = {
      id: -1,
      name: resultForm.name,
      textResponse: resultForm.textResponse,
      itemResponse: null
    };
    this.dungeonBuilderService.addRoomEventToRoom(this.gameID, this.selectedRoom.room.id, roomEvent).toPromise().then(game => {
      if (game) {

        this.game = game;
        this.textEventForm.reset();
        this.dungeonBuilderService.getRoomMatrix(this.gameID).toPromise().then(matrix => {
          if (matrix) {

            this.rooms = matrix;
            this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
          } else {
            this.dialog.open(InfoDialogComponent, {
              data: 'Ein Fehler ist aufgetreten.'
            });
          }
        })
          .catch(() => {
            this.dialog.open(InfoDialogComponent, {
              data: 'Ein Fehler ist aufgetreten.'
            });
          });
      } else {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      }
    })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  addItemEvent() {
    const resultForm = this.itemEventForm.value;

    const roomEvent: RoomEvent = {
      id: -1,
      name: resultForm.name,
      textResponse: null,
      itemResponse: resultForm.itemResponse
    };
    this.dungeonBuilderService.addRoomEventToRoom(this.gameID, this.selectedRoom.room.id, roomEvent).toPromise().then(game => {
      if (game) {

        this.game = game;
        this.itemEventForm.reset();
        this.dungeonBuilderService.getRoomMatrix(this.gameID).toPromise().then(matrix => {
          if (matrix) {

            this.rooms = matrix;
            this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
          } else {
            this.dialog.open(InfoDialogComponent, {
              data: 'Ein Fehler ist aufgetreten.'
            });
          }
        })
          .catch(() => {
            this.dialog.open(InfoDialogComponent, {
              data: 'Ein Fehler ist aufgetreten.'
            });
          });
      } else {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      }
    })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  editTextEvent(event: RoomEvent) {
    const resultForm = this.textEventForm.value;
    event.name = resultForm.name;
    event.textResponse = resultForm.textResponse;

    this.dungeonBuilderService.addRoomEventToRoom(this.gameID, this.selectedRoom.room.id, event).toPromise().then(game => {
      if (game) {

        this.game = game;
        this.textEventForm.reset();
      } else {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      }
    })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  editItemEvent(event: RoomEvent) {
    const resultForm = this.itemEventForm.value;
    event.name = resultForm.name;
    event.itemResponse = resultForm.itemResponse;

    this.dungeonBuilderService.addRoomEventToRoom(this.gameID, this.selectedRoom.room.id, event).toPromise().then(game => {
      if (game) {

        this.game = game;
        this.itemEventForm.reset();
      } else {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      }
    })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  deleteRoomEvent(eventID: number) {
    this.dungeonBuilderService.removeRoomEventFromRoom(this.gameID, this.selectedRoom.room.id, eventID)
      .toPromise().then(game => {
      if (game) {

        this.game = game;
        this.dungeonBuilderService.getRoomMatrix(this.gameID).toPromise().then(matrix => {
          if (matrix) {

            this.rooms = matrix;
            this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
          } else {
            this.dialog.open(InfoDialogComponent, {
              data: 'Ein Fehler ist aufgetreten.'
            });
          }
        })
          .catch(() => {
            this.dialog.open(InfoDialogComponent, {
              data: 'Ein Fehler ist aufgetreten.'
            });
          });
      } else {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      }
    })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Ein Fehler ist aufgetreten.'
        });
      });
  }

  // NPC
  setEditNpcValues(npc: NPC) {
    this.npcForm.controls.name.setValue(npc.name);
  }

  setEditNpcActionValues(action: NPCAction) {
    this.npcActionForm.controls.name.setValue(action.name);
    this.npcActionForm.controls.response.setValue(action.response);
  }

  addNpc() {
    const resultForm = this.npcForm.value;

    const npc: NPC = {
      id: -1,
      name: resultForm.name,
      actions: []
    };
    this.dungeonBuilderService.addNpcToRoom(this.gameID, this.selectedRoom.room.id, npc)
      .toPromise().then(game => {
      this.game = game;
      this.npcTemplateForm.reset();
      this.dungeonBuilderService.getRoomMatrix(this.gameID).toPromise().then(matrix => {
        this.rooms = matrix;
        this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
      });
    });
  }

  editNpc(npc: NPC) {
    const resultForm = this.npcForm.value;
    npc.name = resultForm.name;

    this.dungeonBuilderService.addNpcToRoom(this.gameID, this.selectedRoom.room.id, npc)
      .toPromise().then(game => {
      this.game = game;
      this.npcForm.reset();
    });
  }

  deleteNpc(npcID: number) {
    this.dungeonBuilderService.removeNpcFromRoom(this.gameID, this.selectedRoom.room.id, npcID)
      .toPromise().then(game => {
      this.game = game;
      this.dungeonBuilderService.getRoomMatrix(this.gameID).toPromise().then(matrix => {
        this.rooms = matrix;
        this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
      });
    });
  }

  addNpcAction(npc: NPC) {
    const resultForm = this.npcActionForm.value;

    npc.actions.push({name: resultForm.name, response: resultForm.response});

    this.dungeonBuilderService.addNpcToRoom(this.gameID, this.selectedRoom.room.id, npc)
      .toPromise().then(game => {
      this.game = game;
      this.npcActionForm.reset();
    });
  }

  editNpcAction(action: NPCAction, npc: NPC) {
    const resultForm = this.npcActionForm.value;
    action.name = resultForm.name;
    action.response = resultForm.response;

    this.dungeonBuilderService.addNpcToRoom(this.gameID, this.selectedRoom.room.id, npc)
      .toPromise().then(game => {
      this.game = game;
      this.npcActionForm.reset();
    });
  }

  deleteNpcAction(npc: NPC, actionIndex: number) {
    npc.actions.splice(actionIndex, 1);
    this.dungeonBuilderService.addNpcToRoom(this.gameID, this.selectedRoom.room.id, npc)
      .toPromise().then(game => {
      this.game = game;
    });
  }


  // OBJECT-TEMPLATES
  // ITEM
  setEditItemAttributeTemplateValues(attribute: Attribute) {
    this.itemAttributeTemplateForm.controls.name.setValue(attribute.name);
    this.itemAttributeTemplateForm.controls.value.setValue(attribute.value);
  }

  setEditItemTemplateValues(item: Item) {
    this.itemTemplateForm.controls.name.setValue(item.name);
    this.itemTemplateForm.controls.weight.setValue(item.weight);
  }

  addItemTemplate() {
    const resultForm = this.itemTemplateForm.value;

    const item: Item = {
      id: -1,
      name: resultForm.name,
      weight: resultForm.weight,
      attributes: {attributes: []},
      itemType: resultForm.itemType
    };
    this.dungeonBuilderService.addItemTemplate(this.gameID, item).toPromise().then(game => {
      this.game = game;
      this.itemTemplateForm.reset();
    });
  }

  editItemTemplate(item: Item) {
    const resultForm = this.itemTemplateForm.value;
    item.name = resultForm.name;
    item.weight = resultForm.weight;

    this.dungeonBuilderService.addItemTemplate(this.gameID, item).toPromise().then(game => {
      this.game = game;
      this.itemTemplateForm.reset();
    });
  }

  addItemAttributeTemplate(item: Item) {
    const resultForm = this.itemAttributeTemplateForm.value;

    item.attributes.attributes.push({'name': resultForm.name, 'value': resultForm.value});

    this.dungeonBuilderService.addItemTemplate(this.gameID, item).toPromise().then(game => {
      this.game = game;
      this.itemAttributeTemplateForm.reset();
    });
  }

  editItemAttributeTemplate(attribute: Attribute, item: Item) {
    const resultForm = this.itemAttributeTemplateForm.value;
    attribute.name = resultForm.name;
    attribute.value = resultForm.value;

    this.dungeonBuilderService.addItemTemplate(this.gameID, item).toPromise().then(game => {
      this.game = game;
      this.itemAttributeTemplateForm.reset();
    });
  }

  deleteItemTemplate(itemID: number) {
    this.dungeonBuilderService.removeItemTemplate(this.gameID, itemID)
      .toPromise().then(game => this.game = game);
  }

  deleteItemAttributeTemplate(item: Item, attributeIndex: number) {
    item.attributes.attributes.splice(attributeIndex, 1);
    this.dungeonBuilderService.addItemTemplate(this.gameID, item).toPromise().then(game => {
      this.game = game;
    });
  }

  addItemFromTemplate(item: Item) {
    if (this.selectedRoom && this.selectedRoom.room) {
      const roomItem: Item = {
        ...item,
        id: -1
      };
      this.dungeonBuilderService.addItemToRoom(this.gameID, this.selectedRoom.room.id, roomItem).toPromise().then(game => {
        this.game = game;
        this.dungeonBuilderService.getRoomMatrix(this.gameID).toPromise().then(matrix => {
          this.rooms = matrix;
          this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
        });
      });
    } else {
      this.dialog.open(InfoDialogComponent, {
        data: 'Es muss ein Raum ausgewählt sein!'
      });
    }
  }


  // ROOMEVENT
  setEditTextEventTemplateValues(roomEvent: RoomEvent) {
    this.textEventTemplateForm.controls.name.setValue(roomEvent.name);
    this.textEventTemplateForm.controls.textResponse.setValue(roomEvent.textResponse);
  }

  setEditItemEventTemplateValues(roomEvent: RoomEvent) {
    this.itemEventTemplateForm.controls.name.setValue(roomEvent.name);
  }

  addTextEventTemplate() {
    const resultForm = this.textEventTemplateForm.value;

    const roomEvent: RoomEvent = {
      id: -1,
      name: resultForm.name,
      textResponse: resultForm.textResponse,
      itemResponse: null
    };
    this.dungeonBuilderService.addRoomEventTemplate(this.gameID, roomEvent).toPromise().then(game => {
      this.game = game;
      this.textEventTemplateForm.reset();
    });
  }

  addItemEventTemplate() {
    const resultForm = this.itemEventTemplateForm.value;

    const roomEvent: RoomEvent = {
      id: -1,
      name: resultForm.name,
      textResponse: null,
      itemResponse: resultForm.itemResponse
    };
    this.dungeonBuilderService.addRoomEventTemplate(this.gameID, roomEvent).toPromise().then(game => {
      this.game = game;
      this.itemEventTemplateForm.reset();
    });
  }

  editTextEventTemplate(event: RoomEvent) {
    const resultForm = this.textEventTemplateForm.value;
    event.name = resultForm.name;
    event.textResponse = resultForm.textResponse;

    this.dungeonBuilderService.addRoomEventTemplate(this.gameID, event).toPromise().then(game => {
      this.game = game;
      this.textEventTemplateForm.reset();
    });
  }

  editItemEventTemplate(event: RoomEvent) {
    const resultForm = this.itemEventTemplateForm.value;
    event.name = resultForm.name;
    event.itemResponse = resultForm.itemResponse;

    this.dungeonBuilderService.addRoomEventTemplate(this.gameID, event).toPromise().then(game => {
      this.game = game;
      this.itemEventTemplateForm.reset();
    });
  }

  deleteRoomEventTemplate(eventID: number) {
    this.dungeonBuilderService.removeRoomEventTemplate(this.gameID, eventID)
      .toPromise().then(game => this.game = game);
  }

  addRoomEventFromTemplate(roomEvent: RoomEvent) {
    if (this.selectedRoom && this.selectedRoom.room) {
      const event: RoomEvent = {
        ...roomEvent,
        id: -1
      };
      this.dungeonBuilderService.addRoomEventToRoom(this.gameID, this.selectedRoom.room.id, event)
        .toPromise().then(game => {
        this.game = game;
        this.dungeonBuilderService.getRoomMatrix(this.gameID).toPromise().then(matrix => {
          this.rooms = matrix;
          this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
        });
      });
    } else {
      this.dialog.open(InfoDialogComponent, {
        data: 'Es muss ein Raum ausgewählt sein!'
      });
    }
  }


  // NPC
  setEditNpcTemplateValues(npc: NPC) {
    this.npcTemplateForm.controls.name.setValue(npc.name);
  }

  setEditNpcActionTemplateValues(action: NPCAction) {
    this.npcTemplateActionForm.controls.name.setValue(action.name);
    this.npcTemplateActionForm.controls.response.setValue(action.response);
  }

  addNpcTemplate() {
    const resultForm = this.npcTemplateForm.value;

    const npc: NPC = {
      id: -1,
      name: resultForm.name,
      actions: []
    };
    this.dungeonBuilderService.addNpcTemplate(this.gameID, npc)
      .toPromise().then(game => {
      this.game = game;
      this.npcTemplateForm.reset();
    });
  }

  editNpcTemplate(npc: NPC) {
    const resultForm = this.npcTemplateForm.value;
    npc.name = resultForm.name;

    this.dungeonBuilderService.addNpcTemplate(this.gameID, npc)
      .toPromise().then(game => {
      this.game = game;
      this.npcTemplateForm.reset();
    });
  }

  deleteNpcTemplate(npcID: number) {
    this.dungeonBuilderService.removeNpcTemplate(this.gameID, npcID)
      .toPromise().then(game => this.game = game);
  }

  addNpcActionTemplate(npc: NPC) {
    const resultForm = this.npcTemplateActionForm.value;

    npc.actions.push({name: resultForm.name, response: resultForm.response});

    this.dungeonBuilderService.addNpcTemplate(this.gameID, npc)
      .toPromise().then(game => {
      this.game = game;
      this.npcTemplateActionForm.reset();
    });
  }

  editNpcActionTemplate(action: NPCAction, npc: NPC) {
    const resultForm = this.npcTemplateActionForm.value;
    action.name = resultForm.name;
    action.response = resultForm.response;

    this.dungeonBuilderService.addNpcTemplate(this.gameID, npc)
      .toPromise().then(game => {
      this.game = game;
      this.npcTemplateActionForm.reset();
    });
  }

  deleteNpcActionTemplate(npc: NPC, actionIndex: number) {
    npc.actions.splice(actionIndex, 1);
    this.dungeonBuilderService.addNpcTemplate(this.gameID, npc)
      .toPromise().then(game => {
      this.game = game;
    });
  }

  addNpcFromTemplate(npc: NPC) {
    if (this.selectedRoom && this.selectedRoom.room) {
      const roomNpc: NPC = {
        ...npc,
        id: -1
      };
      this.dungeonBuilderService.addNpcToRoom(this.gameID, this.selectedRoom.room.id, roomNpc)
        .toPromise().then(game => {
        this.game = game;
        this.dungeonBuilderService.getRoomMatrix(this.gameID).toPromise().then(matrix => {
          this.rooms = matrix;
          this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
        });
      });
    } else {
      this.dialog.open(InfoDialogComponent, {
        data: 'Es muss ein Raum ausgewählt sein!'
      });
    }
  }


  // ROOMS
  createRoom(x: number, y: number) {
    const roomInfo = this.createRoomForm.value;

    if (this.rooms[x][y] == Direction.START) {
      this.dungeonBuilderService.createRoom(this.gameID, roomInfo.name, roomInfo.description)
        .toPromise().then(room => {
        if (room) {
          this.rooms[x][y] = {
            room: room,
            north: false,
            east: false,
            south: false,
            west: false,
            startRoom: true
          };
          this.selectRoom(x, y);
          this.createRoomForm.reset();
        }
      }, () => {
        this.dialog.open(ErrorDialogComponent, {
          data: 'Es ist ein Fehler bei der Raumerstellung aufgetreten! Versuchen Sie es bitte erneut.'
        });
      });
    } else {
      const direction: Direction = this.rooms[x][y] as number;
      let room1 = this.selectedRoom;

      this.dungeonBuilderService.createRoom(this.gameID, roomInfo.name, roomInfo.description)
        .toPromise().then((room: Room) => {
        this.dungeonBuilderService.connectRooms(this.gameID, room1.room.id, room.id, direction)
          .toPromise().then((rooms: MatrixRoom[][]) => {
          if (rooms) {
            this.rooms = rooms;
            this.selectRoom(x, y);
            this.createRoomForm.reset();
          }
        });
      }, () => {
        this.dialog.open(ErrorDialogComponent, {
          data: 'Es ist ein Fehler bei der Raumerstellung aufgetreten! Versuchen Sie es bitte erneut.'
        });
      });
    }
  }

  selectRoom(x: number, y: number) {
    this.selectedRoom = this.rooms[x][y] as MatrixRoom;
    this.selectedRoomPosition[0] = x;
    this.selectedRoomPosition[1] = y;
    this.resetRoomAddButtons();

    if (x - 1 >= 0 && this.rooms[x - 1][y] == null) {
      this.rooms[x - 1][y] = Direction.WEST;
    }
    if (x + 1 < this.rooms.length && this.rooms[x + 1][y] == null) {
      this.rooms[x + 1][y] = Direction.EAST;
    }
    if (y - 1 >= 0 && this.rooms[x][y - 1] == null) {
      this.rooms[x][y - 1] = Direction.NORTH;
    }
    if (y + 1 < this.rooms[x].length && this.rooms[x][y + 1] == null) {
      this.rooms[x][y + 1] = Direction.SOUTH;
    }
  }

  selectRoomPathChange(x: number, y: number) {
    this.selectedRoom = this.rooms[x][y] as MatrixRoom;
    this.selectedRoomPosition[0] = x;
    this.selectedRoomPosition[1] = y;
    this.resetPathButtons();

    const west: MatrixRoom = this.rooms[x - 1][y] as MatrixRoom;
    const east: MatrixRoom = this.rooms[x + 1][y] as MatrixRoom;
    const north: MatrixRoom = this.rooms[x][y - 1] as MatrixRoom;
    const south: MatrixRoom = this.rooms[x][y + 1] as MatrixRoom;

    if (x - 1 >= 0 && west && west.room) {
      west.pathChangeDirection = Direction.WEST;
      west.deletePath = this.selectedRoom.west == true;
    }
    if (x + 1 < this.rooms.length && east && east.room) {
      east.pathChangeDirection = Direction.EAST;
      east.deletePath = this.selectedRoom.east == true;
    }
    if (y - 1 >= 0 && north && north.room) {
      north.pathChangeDirection = Direction.NORTH;
      north.deletePath = this.selectedRoom.north == true;
    }
    if (y + 1 < this.rooms[x].length && south && south.room) {
      south.pathChangeDirection = Direction.SOUTH;
      south.deletePath = this.selectedRoom.south == true;
    }
  }

  removePathway(x: number, y: number) {
    const room2: MatrixRoom = this.rooms[x][y] as MatrixRoom;
    this.dungeonBuilderService.removePathway(this.gameID, this.selectedRoom.room.id, room2.room.id).toPromise()
      .then(rooms => {
        if (rooms) {
          this.rooms = rooms;
          this.selectRoomPathChange(x, y);
        } else {
          this.dialog.open(InfoDialogComponent, {
            data: 'Dieser Pfad kann nicht gelöscht werden. Ein Raum muss immer zum Startraum verbunden sein!'
          });
        }
      });
  }

  addPathway(x: number, y: number) {
    const room2: MatrixRoom = this.rooms[x][y] as MatrixRoom;
    this.dungeonBuilderService.connectRooms(this.gameID, this.selectedRoom.room.id, room2.room.id, room2.pathChangeDirection)
      .toPromise()
      .then(rooms => {
        this.rooms = rooms;
        this.selectRoomPathChange(x, y);
      });
  }

  switchPathChange() {
    if (this.togglePathChange == true) {
      this.resetPathButtons();
      this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
    } else {
      this.resetRoomAddButtons();
      this.selectRoomPathChange(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
    }
  }

  unselectRoom() {
    this.resetRoomAddButtons();
    this.resetPathButtons();
    this.selectedRoom = null;
    this.selectedRoomPosition = [];
  }

  editRoom() {
    const resultForm = this.editRoomForm.value;

    this.dungeonBuilderService.editRoom(this.gameID, this.selectedRoom.room.id, resultForm.name, resultForm.description)
      .toPromise().then(rooms => {
      this.rooms = rooms;
      this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
      this.editRoomForm.reset();
    });
  }

  private resetRoomAddButtons() {
    for (let x = 0; x < this.rooms.length; x++) {
      for (let y = 0; y < this.rooms[x].length; y++) {
        if (this.rooms[x][y] >= 1) {
          this.rooms[x][y] = null;
        }
      }
    }
  }

  private resetPathButtons() {
    for (let x = 0; x < this.rooms.length; x++) {
      for (let y = 0; y < this.rooms[x].length; y++) {
        if ((this.rooms[x][y] as MatrixRoom)?.deletePath != null) {
          (this.rooms[x][y] as MatrixRoom).deletePath = null;
        }
        if ((this.rooms[x][y] as MatrixRoom)?.pathChangeDirection) {
          (this.rooms[x][y] as MatrixRoom).pathChangeDirection = null;
        }
      }
    }
  }

  setEditRoomValues() {
    this.editRoomForm.controls.name.setValue(this.selectedRoom.room.name);
    this.editRoomForm.controls.description.setValue(this.selectedRoom.room.description);
  }

  convertItemTypeString(type: string) {
    switch (type) {
      case 'CONSUMABLE':
        return 'Konsumierbar';
      case 'ARMOUR':
        return 'Rüstung';
      case 'WEAPON':
        return 'Waffe';
      case 'ACCESSOIRE':
        return 'Accessoire';
    }
  }

  // WEBSOCKET

  connect() {
    const socket = new SockJS(`${environment.baseUrl}/ws-chat`);
    this.stompClient = Stomp.over(socket);

    const _this = this;
    this.stompClient.connect({}, function(frame) {
      console.log('Connected: ' + frame);
      _this.connectToRooms();
      _this.connectToCharacters();
      _this.otherConnections.push(_this.stompClient.subscribe('/topic/game/' + _this.gameID + '/newChar/', function(message) {
        const char: Character = JSON.parse(message.body);
        _this.game.dungeon.activeCharacters.push(char);
        _this.changeDetectorRefs.detectChanges();
        _this.table.renderRows();
        _this.connectToCharacter(char);
      }));

      _this.otherConnections.push(_this.stompClient.subscribe('/topic/game/' + _this.gameID + '/delChar/', function(message) {
        const char: number = JSON.parse(message.body);
        for (let i = 0; i < _this.game.dungeon.activeCharacters.length; i++) {
          if (_this.game.dungeon.activeCharacters[i].id == char) {
            _this.game.dungeon.activeCharacters.splice(i, 1);
            _this.changeDetectorRefs.detectChanges();
            _this.table.renderRows();
            break;
          }
        }
        if (_this.selectedCharacter != null && _this.selectedCharacter.id == char) {
          _this.selectedCharacter = null;
          _this.selectedCharacterIndex = null;
        }
        _this.disconnectFromCharacter(char);
      }));
      _this.otherConnections.push(_this.stompClient.subscribe('/topic/game/' + _this.gameID + '/roomsWM', function() {
        _this.dungeonBuilderService.getRoomMatrix(_this.gameID).toPromise().then(e => {
          _this.rooms = e;
          _this.connectToRooms();
        });
      }));
      _this.stompClient.subscribe(`/topic/dm/` + _this.masterID, function(message) {
        _this.playerMessages.push(message.body);
      });
      _this.stompClient.subscribe(`/topic/cube/` + _this.masterID, function(message) {
        _this.playerMessages.push(message.body);
      });
      _this.stompClient.subscribe(`/topic/deathwatch/` + _this.gameID, function(message) {
        _this.playerMessages.push(message.body);
      });
    });
  }

  disconnectFromAll() {
    this.roomConnections.forEach(x => x.unsubscribe());
    this.characterConnections.forEach(x => x.unsubscribe());
    this.otherConnections.forEach(x => x.unsubscribe());
    if (this.stompClient != null) {
      this.stompClient.disconnect();
    }
    console.log('Disconnected!');
  }

  roomConnections: any[] = [];
  characterConnections: any[] = [];
  otherConnections: any[] = [];

  connectToRooms() {
    for (let i = 0; i < this.roomConnections.length; i++) {
      this.roomConnections[i].unsubscribe();
    }
    this.roomConnections = [];
    const _this = this;
    let topicName: string = '/topic/game/' + this.gameID + '/room/';
    for (let i = 0; i < this.rooms.length; i++) {
      for (let j = 0; j < this.rooms[0].length; j++) {
        const maRoom: MatrixRoom | Direction = this.rooms[i][j];
        if (maRoom != null && _this.instanceOfMatrixRoom(maRoom)) {
          this.roomConnections.push(_this.stompClient.subscribe(topicName + maRoom.room.id, function(message) {
            const room: Room = JSON.parse(message.body);
            _this.refreshRoom(room);
          }));
        }
      }
    }
  }

  disconnectFromRooms() {
    this.roomConnections.forEach(x => {
      x.unsubscribe();
    });
    this.roomConnections = [];
  }

  connectToCharacters() {
    const _this = this;
    let topicName: string = '/topic/game/' + this.gameID + '/character/';
    for (let i = 0; i < this.game.dungeon.activeCharacters.length; i++) {
      let character: Character = this.game.dungeon.activeCharacters[i];
      this.characterConnections.push(_this.stompClient.subscribe(topicName + character.id, function(message) {
        const char: Character = JSON.parse(message.body);
        console.log('Connected' + char);
        _this.refreshCharacter(char);
      }));
    }
  }

  connectToCharacter(char: Character) {
    const _this = this;
    let topicName: string = '/topic/game/' + this.gameID + '/character/';
    this.characterConnections.push(_this.stompClient.subscribe(topicName + char.id, function(message) {
      const char: Character = JSON.parse(message.body);
      _this.refreshCharacter(char);
    }, {id: char.id}));
  }

  disconnectFromCharacter(charID: number) {
    for (let i = 0; i < this.characterConnections.length; i++) {
      if (this.characterConnections[i].id == charID) {
        this.characterConnections[i].unsubscribe();
        this.characterConnections.splice(i, 1);
        break;
      }
    }
  }


  // CATCH CHANGES
  refreshRoom(room: Room) {
    for (let i = 0; i < this.rooms.length; i++) {
      for (let j = 0; j < this.rooms[0].length; j++) {
        const maRoom: MatrixRoom | Direction = this.rooms[i][j];
        if (maRoom != null && this.instanceOfMatrixRoom(maRoom)) {
          if (maRoom.room.id == room.id) {
            maRoom.room = room;
            this.refreshCharacterLocations(maRoom.room);
            return;
          }
        }
      }
    }
  }

  @ViewChild('MatTab') characterInfoTable: MatTabLabel;

  refreshCharacterLocations(room: Room) {
    for (let i = 0; i < room.presentCharacters.length; i++) {
      for (let j = 0; j < this.activeCharacterSource.length; j++) {
        if (room.presentCharacters[i] == this.activeCharacterSource[j].id) {
          this.activeCharacterSource[j].location = room;
        }
      }
    }
  }


  refreshCharacter(char: Character) {
    for (let i = 0; i < this.game.dungeon.activeCharacters.length; i++) {
      let chosenCharacter: Character = this.game.dungeon.activeCharacters[i];
      if (chosenCharacter.id == char.id) {
        this.game.dungeon.activeCharacters[i] = char;
        //this.game.dungeon.activeCharacters[this.selectedCharacterIndex] = char;
        this.selectedCharacter = char;
        return;
      }
    }
  }


  instanceOfMatrixRoom(object: any): object is MatrixRoom {
    try {
      return 'room' in object;
    } catch (e) {
      return false;
    }
  }
}
