import { TestBed } from '@angular/core/testing';

import { DungeonPlayerService } from './dungeon-player.service';

describe('DungeonPlayerService', () => {
  let service: DungeonPlayerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DungeonPlayerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
