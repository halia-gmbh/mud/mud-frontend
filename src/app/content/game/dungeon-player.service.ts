import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Character} from '../../shared/interfaces/character/character';
import {environment} from '../../../environments/environment';
import {api} from '../../shared/api';
import {Room} from '../../shared/interfaces/room/room';
import {Direction} from '../../shared/interfaces/room/direction';
import {RoomEvent} from '../../shared/interfaces/room/room-event';

@Injectable({
  providedIn: 'root'
})
export class DungeonPlayerService {

  constructor(private httpClient: HttpClient) {
  }

  useItem(gameID: number, characterID: number, itemID: number) {
    return this.httpClient.post<Character>(`${environment.baseUrl}${api.useItem_GameID_CharacterID_ItemID}${gameID}/${characterID}/${itemID}`, '')
  }
  unEquipItem(gameID: number, characterID: number, itemID: number) {
    return this.httpClient.post<Character>(`${environment.baseUrl}${api.unEquipItem_GameID_CharacterID_ItemID}${gameID}/${characterID}/${itemID}`, '')
  }
  move(gameID: number, characterID: number, direction: Direction) {
    return this.httpClient.post<Room>(`${environment.baseUrl}${api.moveToRoom_GameID_CharacterID_Direction}${gameID}/${characterID}/${direction}`, '')
  }
  pickUpItem(gameID: number, characterID: number, itemName: string) {
    return this.httpClient.post<Character>(`${environment.baseUrl}${api.pickUpItem_GameID_CharacterID_ItemName}${gameID}/${characterID}/${itemName}`, '')
  }
  discardItem(gameID: number, characterID: number, itemID: number) {
    return this.httpClient.post<Character>(`${environment.baseUrl}${api.discardItem_GameID_CharacterID_ItemID}${gameID}/${characterID}/${itemID}`, '')
  }
  interactWithRoomEvent(gameID: number, characterID: number, eventName: string) {
    return this.httpClient.post<RoomEvent>(`${environment.baseUrl}${api.interactWithRoomEvent_GameID_CharacterID_EventName}${gameID}/${characterID}/${eventName}`, '')
  }
  getCharactersInRoom(gameID: number, roomID: number) {
    return this.httpClient.post<Character[]>(`${environment.baseUrl}${api.getCharactersInRoom_GameID_RoomID}${gameID}/${roomID}`, '')
  }
  quitGame(gameID: number, characterID: number) {
    return this.httpClient.post<boolean>(`${environment.baseUrl}${api.quitGame}${gameID}/${characterID}`, '')
  }
  setPlayerDeathWatch(gameID: number, characterID: number) {
    return this.httpClient.post<boolean>(`${environment.baseUrl}${api.setPlayerDeathWatch_GameID_CharacterID}${gameID}/${characterID}`, '')
  }
  resetPlayerDeathWatch(gameID: number, characterID: number) {
    return this.httpClient.post<boolean>(`${environment.baseUrl}${api.resetPlayerDeathWatch_CharacterID}${gameID}/${characterID}`, '')
  }

}
