import {Component, HostListener, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Stomp} from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import {environment} from '../../../../environments/environment';
import {MatDialog} from '@angular/material/dialog';
import {InfoDialogComponent} from '../../../tools/dialogs/info-dialog/info-dialog.component';
import {ActivatedRoute, Router} from '@angular/router';
import {Message, MessageType} from '../../../shared/interfaces/message';
import {DungeonPlayerService} from '../dungeon-player.service';
import {DungeonService} from '../../overview/dungeon.service';
import {Game} from '../../../shared/interfaces/game';
import {DungeonBuilderService} from '../../builder/dungeon-builder.service';
import {Direction} from '../../../shared/interfaces/room/direction';
import {Character} from '../../../shared/interfaces/character/character';
import {ErrorDialogComponent} from '../../../tools/dialogs/error-dialog/error-dialog.component';
import {Item, ItemTypeEnum} from '../../../shared/interfaces/room/item';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatrixRoom, Room} from '../../../shared/interfaces/room/room';
import {NPC, NPCAction} from '../../../shared/interfaces/room/npc';
import {routeConsts} from '../../../shared/routes';


export const commands = {
  help: 'hilfe',
  moveTo: 'laufen.',
  interactNpc: 'npc.',
  interactEvent: 'event.',
  cube: 'würfeln',
  useItem: 'item.',
  lookAround: 'umsehen',
  chatPlayer: 'spieler.',
  chatRoom: 'raum.'
}

@Component({
  selector: 'hd-dungeon-player',
  templateUrl: './dungeon-player.component.html',
  styleUrls: ['./dungeon-player.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class DungeonPlayerComponent implements OnInit {

  constructor(private dialog: MatDialog,
              private route: ActivatedRoute,
              private dungeonPlayerService: DungeonPlayerService,
              private dungeonBuilderService: DungeonBuilderService,
              private dungeonService: DungeonService,
              private router: Router) {
  }

/*
  @HostListener('window:beforeunload')
  unloadHandler(event) {
    if(!this.correctlyEnded) {
      this.dungeonPlayerService.setPlayerDeathWatch(this.gameID, this.characterID).toPromise().then();
    }
  } */



  armour = 'ARMOUR';
  weapon = 'WEAPON';
  accessoire = 'ACCESSOIRE';

  expandedColumn: Item | null;
  itemTypes = [{'CONSUMABLE': 'Konsumierbar'}, {'ARMOUR': 'Rüstung'}, {'WEAPON': 'Waffe'}, {'ACCESSOIRE': 'Accesssoire'}];
  itemColumnsToDisplay = ['name', 'weight', 'itemType', 'buttons'];


  private stompClient;
  gameID: number;
  characterID: number;
  userID: number;

  game: Game; // Nur initial benutzt
  selectedCharacter: Character = {
    id: 0,
    name: '',
    active: false,
    attributes: {
      attributes: []
    },
    weapon: {
      id: 0,
      name: '',
      weight: 0,
      attributes: {
        attributes: []
      },
      itemType: ItemTypeEnum.WEAPON
    },
    armour: {
      id: 0,
      name: '',
      weight: 0,
      attributes: {
        attributes: []
      },
      itemType: ItemTypeEnum.ARMOUR
    },
    accessoire: {
      id: 0,
      name: '',
      weight: 0,
      attributes: {
        attributes: []
      },
      itemType: ItemTypeEnum.ACCESSOIRE
    },
    inventory: [],
    characterRace: {
      id: 0,
      name: '',
      description: '',
      attributes: {
        attributes: []
      }
    },
    characterClass: {
      id: 0,
      name: '',
      description: '',
      attributes: {
        attributes: []
      }
    },
    location: null
  }

  chatMessages: string[] = [];
  dmMessages: string[] = [];

  chatForm: FormGroup;
  dmChatForm: FormGroup;

  equipArmourForm: FormGroup;
  equipWeaponForm: FormGroup;
  equipAccessoireForm: FormGroup;

  rooms: (MatrixRoom)[][] = [[]];
  selectedRoom: MatrixRoom;

  correctlyEnded = false;

  resetDeathwatch(gameID, characterID) {
    console.log("RESET PLAYER DW")
    const _this = this;
    this.dungeonPlayerService.resetPlayerDeathWatch(gameID, characterID).toPromise().then();
    if(!this.correctlyEnded)
      window.setTimeout(function () {
      _this.resetDeathwatch(gameID, characterID);
    }, 10000);
  }

  ngOnInit(): void {
    this.gameID = +this.route.snapshot.paramMap.get('gameID');
    this.characterID = +this.route.snapshot.paramMap.get('playerID');
    this.userID = +sessionStorage.getItem('userID');
    this.dungeonService.getGame(this.gameID).toPromise().then((game: Game) => {
      this.game = game;
      this.selectedCharacter = this.getCharacter(this.characterID, game);
      this.dungeonBuilderService.getRoomMatrix(this.gameID).toPromise()
        .then(matrix => {
          this.rooms = matrix
          this.selectRoom(this.selectedCharacter.location.id)
        })
    });

    //this.dungeonPlayerService.resetPlayerDeathWatch(this.gameID).toPromise().then()

    this.chatForm = new FormGroup({
      message: new FormControl('', Validators.required)
    });
    this.dmChatForm = new FormGroup({
      message: new FormControl('', Validators.required)
    });

    this.equipArmourForm = new FormGroup({
      item: new FormControl('', Validators.required)
    });
    this.equipWeaponForm = new FormGroup({
      item: new FormControl('', Validators.required)
    });
    this.equipAccessoireForm = new FormGroup({
      item: new FormControl('', Validators.required)
    });

    this.setInteractionHelp();
    this.setChatHelp();

    this.connect();
    this.resetDeathwatch(this.gameID, this.characterID);
  }

  selectRoom(roomID: number) {
    for (let i = 0; i<this.rooms.length; i++) {
      for (let j = 0; j<this.rooms[i].length; j++) {
        if (roomID == this.rooms[i][j]?.room.id)
          this.selectedRoom = this.rooms[i][j]
      }
    }
  }

  ngOnDestroy() {
    this.disconnect();
  }


  setInteractionHelp() {
    this.dmMessages.push('Mögliche befehle:\n' +
      '  - laufen.[norden, osten, süden, westen]\n' +
      '  - npc.[name].[aktion]\n' +
      '  - event.[name]\n' +
      '  - würfeln\n' +
      '  - item.[name]\n' +
      '  - umsehen\n' +
      '  - und alles andere was du dir vorstellen kannst!');
  }

  setChatHelp() {
    this.chatMessages.push('Mögliche Befehle:\n' +
      '  - raum.[nachricht]\n' +
      '  - spieler.[name].[nachricht]');
  }

  switchRoom(room: Room) {
    this.selectedCharacter.location = room;
    this.selectRoom(room.id)
    this.dmChatForm.reset();
    this.dmMessages.push(room.name + ' betreten.\n' +
      'Beschreibung: ' + room.description);
    this.changeRoomSubscription(room);
  }

  interact() {
    const resultForm: string = this.dmChatForm.value.message;

    const message: Message = {
      senderName: this.selectedCharacter.name,
      receiverName: '',
      content: '',
      type: null,
      roomID: this.selectedCharacter.location.id,
      gameID: this.gameID
    };

    switch (true) {
      case(resultForm == commands.help):
        this.setInteractionHelp();
        this.dmChatForm.reset();
        break;
      case(resultForm.startsWith(commands.moveTo)):
        const s = resultForm.slice(commands.moveTo.length);
        if (s == 'norden') {
          this.dungeonPlayerService.move(this.gameID, this.characterID, Direction.NORTH).toPromise().then(room => {
            this.switchRoom(room)
            this.dmChatForm.reset();
          }).catch(() => {
            this.dialog.open(InfoDialogComponent, {
              data: 'Es gibt in diese Richtung keinen Raum mit einer Verbindung!'
            });
          });
        }
        if (s == 'osten') {
          this.dungeonPlayerService.move(this.gameID, this.characterID, Direction.EAST).toPromise().then(room => {
            this.switchRoom(room)
            this.dmChatForm.reset();
          }).catch(() => {
            this.dialog.open(InfoDialogComponent, {
              data: 'Es gibt in diese Richtung keinen Raum mit einer Verbindung!'
            });
          });
        }
        if (s == 'süden') {
          this.dungeonPlayerService.move(this.gameID, this.characterID, Direction.SOUTH).toPromise().then(room => {
            this.switchRoom(room)
            this.dmChatForm.reset();
          }).catch(() => {
            this.dialog.open(InfoDialogComponent, {
              data: 'Es gibt in diese Richtung keinen Raum mit einer Verbindung!'
            });
          });
        }
        if (s == 'westen') {
          this.dungeonPlayerService.move(this.gameID, this.characterID, Direction.WEST).toPromise().then(room => {
            this.switchRoom(room)
            this.dmChatForm.reset();
          }).catch(() => {
            this.dialog.open(InfoDialogComponent, {
              data: 'Es gibt in diese Richtung keinen Raum mit einer Verbindung!'
            });
          });
        }
        break;
      case(resultForm.startsWith(commands.interactNpc)):
        const npcNameAction = resultForm.slice(commands.interactNpc.length).split('.', 2);
        const npc: NPC = this.getNpc(npcNameAction[0], this.selectedCharacter.location)
        if (npc && npcNameAction.length == 1) {
          this.dmMessages.push('Vielleicht findest du die richtigen Stichpunkte, mit denen du mit diesem NPC sprechen kannst.');
          this.dmChatForm.reset();
        }
        else if (npc && npcNameAction.length == 2) {
          const action = this.getNpcAction(npcNameAction[1], npc)
          if (action) {
            this.dmMessages.push(npc.name + ': ' + action.response)
            this.dmChatForm.reset();
          } else {
            this.dmMessages.push('Der NPC zeigt keine Reaktion, versuche es erneut.')
            this.dmChatForm.reset();
          }
        }
        this.dmChatForm.reset();
        break;
      case(resultForm.startsWith(commands.interactEvent)):
        const eventName = resultForm.slice(commands.interactEvent.length)
        this.dungeonPlayerService.interactWithRoomEvent(this.gameID, this.characterID, eventName).toPromise()
          .then(msg => {
            if(msg.textResponse) {
              this.dmMessages.push(msg.textResponse)
              this.dmChatForm.reset();
            }
            else this.dmMessages.push('Ein Item liegt nun im Raum. Sieh dich doch mal um!')
          }).catch(() => {
          this.dialog.open(InfoDialogComponent, {
            data: 'Ein Fehler ist aufgetreten.'
          });
        })
        break;
      case(resultForm == commands.cube):
        message.type = MessageType.CubeEvent;
        this.dungeonService.chat(message).toPromise().then(() => {
          this.dmChatForm.reset();
        });
        break;
      case(resultForm.startsWith(commands.useItem)):
        const itemName = resultForm.slice(commands.useItem.length);
        this.dungeonPlayerService.pickUpItem(this.gameID, this.characterID, itemName).toPromise()
          .then(character => {
            this.selectedCharacter = character;
            this.dmMessages.push('Item erfolgreich genommen.');
            this.dmChatForm.reset();
          }).catch(() => {
            this.dialog.open(InfoDialogComponent, {
              data: 'Ein Fehler ist aufgetreten.'
            });
          });
        break;
      case(resultForm == commands.lookAround):
        const room = this.selectedCharacter.location;

        this.dungeonPlayerService.getCharactersInRoom(this.gameID, this.selectedCharacter.location.id)
          .toPromise()
          .then(characters => {
            let msg: string = 'Raumname: ' + room.name + '\n' +
              'Raumbeschreibung: ' + room.description + '\n' +
              'Items:\n';
            if (room.items.length == 0) {
              msg = msg + ' - Keine\n';
            }
            for (let i = 0; i < room.items.length; i++) {
              msg = msg + ' - ' + room.items[i].name + ' Gewicht: ' + room.items[i].weight + '\n';
            }

            msg = msg + 'NPCs:\n';
            if (room.npcs.length == 0) {
              msg = msg + ' - Keine\n';
            }
            for (let i = 0; i < room.npcs.length; i++) {
              msg = msg + ' - ' + room.npcs[i].name + '\n';
            }

            msg = msg + 'Events:\n';
            if (room.roomEvents.length == 0) {
              msg = msg + ' - Keine\n';
            }
            for (let i = 0; i < room.roomEvents.length; i++) {
              msg = msg + ' - ' + room.roomEvents[i].name + '\n';
            }

            msg = msg + 'Spieler:\n';
            if (characters.length == 1) {
              msg = msg + ' - Keine\n';
            }
            characters.forEach(c => {
              if (c.id != this.selectedCharacter.id) {
                msg = msg + ' - ' + c.name + ' - ' + c.characterRace.name + '\n';
              }
            })
            this.dmMessages.push(msg);
            this.dmChatForm.reset();
          }).catch(() => {
          this.dialog.open(InfoDialogComponent, {
            data: 'Ein Fehler ist aufgetreten.'
          });
        });
        break;
      default:
        message.type = MessageType.PlayerToDm;
        message.content = resultForm;
        this.dmMessages.push('Du: ' + message.content);
        this.dungeonService.chat(message).toPromise().then(() => {
          this.dmChatForm.reset();
        }).catch(() => {
          this.dialog.open(InfoDialogComponent, {
            data: 'Ein Fehler ist aufgetreten.'
          });
        });

    }

  }


  chat() {
    const chatValues: string = this.chatForm.value.message;

    const message: Message = {
      senderName: this.selectedCharacter.name,
      receiverName: '',
      content: '',
      type: null,
      roomID: this.selectedCharacter.location.id,
      gameID: this.gameID
    };

    if (chatValues.startsWith(commands.chatRoom)) {
      const m = chatValues.slice(commands.chatRoom.length);
      this.chatMessages.push('Du: ' + m);
      message.content = m;
      message.type = MessageType.ChatTalk;
      this.dungeonService.chat(message).toPromise().then(() => this.chatForm.reset());
    }
    else if (chatValues.startsWith(commands.chatPlayer)) {
      const splitMessage = chatValues.split('.');
      let msg: string = '';
      for (let i = 2; i<splitMessage.length; i++) {
        if (i<splitMessage.length-1)
          msg = msg + splitMessage[i] + '.'
        else msg = msg +splitMessage[i]
      }
      this.chatMessages.push(splitMessage[1] + ' angeflüstert:\n' + msg);
      message.receiverName = splitMessage[1];
      message.type = MessageType.ChatWhisper;
      message.content = msg;
      this.dungeonService.chat(message).toPromise().then(() => this.chatForm.reset());
    } else {
      this.dialog.open(InfoDialogComponent, {
        data: 'Eingegebener Befehl existiert nicht.'
      })
    }
  }



  connect() {
    const socket = new SockJS(`${environment.baseUrl}/ws-chat`);
    this.stompClient = Stomp.over(socket);

    const _this = this;
    this.stompClient.connect({}, function(frame) {
      console.log('Connected: ' + frame);

      _this.stompClient.subscribe(`/topic/player/` + _this.characterID, function(message) {
        _this.chatMessages.push(message.body);
      });
      _this.stompClient.subscribe(`/topic/dm/` + _this.characterID, function(message) {
        _this.dmMessages.push(message.body);
      });
      _this.stompClient.subscribe(`/topic/cube/` + _this.characterID, function(message) {
        _this.dmMessages.push(message.body);
      });
      _this.connectRefreshing();
      _this.connectChangeDM();
      _this.changeRoomSubscription(_this.selectedCharacter.location);
      _this.stompClient.subscribe(`/topic/game/` + _this.gameID + "/roomsWM", function() {
        _this.dungeonBuilderService.getRoomMatrix(_this.gameID).toPromise().then(e => {
          _this.rooms = e;
          _this.switchRoom(_this.findRoomWithID(_this.selectedRoom.room.id))
        });
      });
      _this.stompClient.subscribe(`/topic/endGame/` + _this.gameID, function() {
        _this.router.navigate([routeConsts.overviewNav])
      });
    });
  }

  findRoomWithID(roomID : number) : Room {
    for(let i = 0; i<this.rooms.length; i++) {
      for(let j = 0; j<this.rooms[i].length; j++) {
        if(this.rooms[i][j] != null && this.rooms[i][j].room.id == roomID) {
          return this.rooms[i][j].room;
        }
      }
    }
    return null;
  }

  connectRefreshing() {
    const _this = this;
    this.stompClient.subscribe('/topic/game/' + this.gameID + '/character/' + this.characterID, function (message) {
      _this.selectedCharacter = JSON.parse(message.body);
    });
  }
  connectChangeDM() {
    const _this = this;
    this.stompClient.subscribe('/topic/game/' + this.gameID + '/changeDM/' + this.characterID, function () {
      _this.router.navigate([routeConsts.masterNavGameID+_this.gameID])
      _this.dialog.open(InfoDialogComponent, {
        data: 'Du bist zum neuen Dungeon Master erwählt worden. Erfülle deine Aufgabe mit Würde und Kreativität.'
      })
    });
  }

  currentRoomSubscription : any;

  changeRoomSubscription(room : Room) {
    if(this.currentRoomSubscription != null) {
      this.currentRoomSubscription.unsubscribe();
    }
    console.log("CHANGED ROOM SUB")
    const _this = this;
    const roomID : number = room.id;
    _this.selectRoom(roomID);
    this.currentRoomSubscription = this.stompClient.subscribe('/topic/game/' + this.gameID + '/room/' + roomID, function (message) {
      const room : Room  = JSON.parse(message.body)
      let subChange : boolean = false;
      if(_this.selectedCharacter.location.id != room.id) {
        subChange = true;
      }
      _this.selectedCharacter.location = room;
      if(subChange == true)
        _this.changeRoomSubscription(room);
    });
  }

  disconnect() {
    if (this.stompClient != null) {
      this.stompClient.disconnect();
    }
    console.log('Disconnected!');
  }

  // Character Infos
  unEquipItem(itemID: number) {
    this.dungeonPlayerService.unEquipItem(this.gameID, this.selectedCharacter.id, itemID)
      .toPromise()
      .then(character => {
        this.selectedCharacter = character;
      });
  }

  equipPlayerArmour() {
    const resultForm = this.equipArmourForm.value;

    this.dungeonPlayerService.useItem(this.gameID, this.selectedCharacter.id, resultForm.item.id).toPromise()
      .then(character => {
        this.selectedCharacter = character;
        this.equipArmourForm.reset();
      });
  }

  equipPlayerWeapon() {
    const resultForm = this.equipWeaponForm.value;

    this.dungeonPlayerService.useItem(this.gameID, this.selectedCharacter.id, resultForm.item.id).toPromise()
      .then(character => {
        this.selectedCharacter = character;
        this.equipWeaponForm.reset();
      });
  }

  equipPlayerAccessoire() {
    const resultForm = this.equipAccessoireForm.value;

    this.dungeonPlayerService.useItem(this.gameID, this.selectedCharacter.id, resultForm.item.id).toPromise()
      .then(character => {
        this.selectedCharacter = character;
        this.equipAccessoireForm.reset();
      });
  }

  useConsumable(itemID: number) {
    this.dungeonPlayerService.useItem(this.gameID, this.characterID, itemID).toPromise()
      .then(character => {
        this.selectedCharacter = character;
        this.dmMessages.push('Item wurde konsumiert.')
      })
  }

  discardItem(itemID: number) {
    this.dungeonPlayerService.discardItem(this.gameID, this.selectedCharacter.id, itemID)
      .toPromise()
      .then(character => {
        this.selectedCharacter = character;
      });
  }

  filterPlayerEquipment(items: Item[], itemType: string) {
    return items.filter(i => i.itemType.toString() === itemType);
  }

  convertItemTypeString(type: string) {
    switch (type) {
      case 'CONSUMABLE':
        return 'Konsumierbar';
      case 'ARMOUR':
        return 'Rüstung';
      case 'WEAPON':
        return 'Waffe';
      case 'ACCESSOIRE':
        return 'Accessoire';
    }
  }

  private getCharacter(id: number, game: Game): Character {
    let char: Character;
    game.dungeon.activeCharacters.forEach(c => {
      if (c.id == id) {
        char = c;
      }
    });
    if (!char) {
      this.dialog.open(ErrorDialogComponent, {
        data: 'Ein Fehler ist aufgetreten!'
      })
    }
    return char
  }

  private getNpc(name: string, room: Room) {
    let npc;
    room.npcs.forEach(n => {
      if (name == n.name)
        npc = n
    })
    return npc;
  }

  private getNpcAction(name: string, npc: NPC): NPCAction {
    let action: NPCAction;
    npc.actions.forEach(a => {
      if (name == a.name)
        action = a
    })
    return action
  }

  quitGame() {
    this.correctlyEnded = true;
    this.dungeonPlayerService.quitGame(this.gameID, this.characterID).toPromise()
      .then(ans => {
        if (ans) {
          this.correctlyEnded = true;
          this.router.navigate([routeConsts.overviewNav])
        }

        else {
          this.dialog.open(InfoDialogComponent, {
            data: 'Es ist ein kritischer Fehler aufgetreten. Bitte wende dich an den Admin.'
          })
        }
      })
      .catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Es ist ein kritischer Fehler aufgetreten. Bitte wende dich an den Admin.'
        })
      })
  }

}
