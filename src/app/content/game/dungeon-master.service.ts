import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {api} from '../../shared/api';
import {Character} from '../../shared/interfaces/character/character';
import {Item} from '../../shared/interfaces/room/item';

@Injectable({
  providedIn: 'root'
})
export class DungeonMasterService {

  constructor(private httpClient: HttpClient) { }

  setAttribute(gameID: number, characterID: number, attIndex: number, attName: string, attValue: number) {
    return this.httpClient.post<Character>(`${environment.baseUrl}${api.setAttribute_GameID_CharacterID_AttIndex_AttName_Value}${gameID}/${characterID}/${attIndex}/${attName}/${attValue}`,'')
  }
  deleteAttribute(gameID: number, characterID: number, attName: string) {
    return this.httpClient.post<Character>(`${environment.baseUrl}${api.deleteAttribute_GameID_CharacterID_AttName}${gameID}/${characterID}/${attName}`,'')
  }

  setItem(gameID: number, characterID: number, item: Item) {
    return this.httpClient.post<Character>(`${environment.baseUrl}${api.setItem_GameID_CharacterID}${gameID}/${characterID}`,item)
  }
  deleteItem(gameID: number, characterID: number, itemID: number) {
    return this.httpClient.post<Character>(`${environment.baseUrl}${api.deleteItem_GameID_CharacterID_ItemID}${gameID}/${characterID}/${itemID}`,'')
  }

  placePlayer(gameID: number, characterID: number, roomID) {
    return this.httpClient.post<Character>(`${environment.baseUrl}${api.placePlayer_GameID_CharacterID_RoomID}${gameID}/${characterID}/${roomID}`,'')
  }

  endDungeon(gameID: number) {
    return this.httpClient.post<Character>(`${environment.baseUrl}${api.endGame_GameID}${gameID}`,'')
  }
  resetDeathWatch(gameID: number) {
    return this.httpClient.post<void>(`${environment.baseUrl}${api.resetDeathWatch_GameID}${gameID}`,'')
  }
  setDeathWatch(gameID: number) {
    return this.httpClient.post<void>(`${environment.baseUrl}${api.setDeathWatch_GameID}${gameID}`,'')
  }

  changeDungeonMaster(gameID: number, characterID: number) {
    return this.httpClient.post<boolean>(`${environment.baseUrl}${api.changeDM_GameID_CharacterID}${gameID}/${characterID}`,'')
  }
}
