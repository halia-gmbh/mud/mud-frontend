import { Component, OnInit } from '@angular/core';
import {AdminService} from "../admin.service";
import {Game} from "../../../shared/interfaces/game";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Account} from '../../../shared/interfaces/account';

@Component({
  selector: 'hd-admin-overview',
  templateUrl: './admin-overview.component.html',
  styleUrls: ['./admin-overview.component.scss']
})
export class AdminOverviewComponent implements OnInit {
  allAccounts: Account[]
  constructor(private adminService: AdminService) { }
  displayedColumns: string[] = ['email'];
  chatForm: FormGroup;

  ngOnInit(): void {
    this.chatForm = new FormGroup({
      message: new FormControl('', Validators.required)
    });
    this.adminService.getAllAccounts()
      .toPromise()
      .then((allAcountss: Account[]) => {
        console.log(allAcountss)
        this.allAccounts = allAcountss;
      });
  }

  deleteAccount(id: number) {
    console.log(id)
    this.adminService.deleteAccount(id).toPromise().then()

  }
  verifyAccount(id: number) {
    this.adminService.verifyAccount(id).toPromise().then()

  }

  chat() {
    const msg = this.chatForm.value.message;
    this.adminService.adminMessage(msg).toPromise().then();
  }
}
