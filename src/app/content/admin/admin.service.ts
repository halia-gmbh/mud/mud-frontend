import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

import {Injectable} from "@angular/core";
import {environment} from "../../../environments/environment";
import {api} from "../../shared/api";
import {Admin} from "../../shared/interfaces/admin";
import {Account} from '../../shared/interfaces/account';
import {map} from "rxjs/operators";
@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private httpClient: HttpClient, private router: Router) {
  }

  login(password: string) {
    return this.httpClient.post<Account>(`${environment.baseUrl}${api.adminLogin}`,password)
      .pipe(
        map((userData: Account) => {
          sessionStorage.setItem('username', 'admin');
          sessionStorage.setItem('userID', String(userData.id));
          let tokenStr = 'Bearer ' + userData.token;
          sessionStorage.setItem('token', tokenStr);
          return userData;
    })
      );
  }
  getAllAccounts(){
    return this.httpClient.post<Account[]>(`${environment.baseUrl}${api.adminGetAllAccouts}`,'');
  }
  deleteAccount(userID: number){
    return this.httpClient.post<boolean>(`${environment.baseUrl}${api.adminDeleteAccount_AccountID}${userID}`,'');
  }
  verifyAccount(userID: number){
    return this.httpClient.post<boolean>(`${environment.baseUrl}${api.adminVerifyAccount_AccountID}${userID}`,'');
  }
  adminMessage(msg : string) {
    return this.httpClient.post<boolean>(`${environment.baseUrl}${api.adminMessage}`, msg);
  }
}
