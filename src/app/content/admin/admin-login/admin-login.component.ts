import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {routeConsts} from "../../../shared/routes";
import {ErrorDialogComponent} from "../../../tools/dialogs/error-dialog/error-dialog.component";
import {AdminService} from "../admin.service";
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'hd-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.scss']
})
export class AdminLoginComponent implements OnInit {

  loginForm: FormGroup

  constructor(private adminService: AdminService,
              private router: Router,
              private dialog: MatDialog) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      password: new FormControl('', Validators.required)
    });
  }

  login() {
    const resultForm = this.loginForm.value;
    if (resultForm.password) {
      this.adminService.login(resultForm.password).toPromise()
        .then(acc => {
          this.router.navigate([routeConsts.adminOverviewNav])
        })
        .catch(() => {
          this.dialog.open(ErrorDialogComponent, {
            data: 'Passwort ist falsch.'
          })
        })
    }
  }

}
