import {Component, HostListener, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../auth/auth.service';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {ErrorDialogComponent} from '../../tools/dialogs/error-dialog/error-dialog.component';
import {InfoDialogComponent} from '../../tools/dialogs/info-dialog/info-dialog.component';
import {routeConsts} from '../../shared/routes';

@Component({
  selector: 'hd-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  registerForm: FormGroup;

  constructor(private authService: AuthService, private router: Router, private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required)
    });
    this.registerForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required)
    });
  }

  login(): void {
    const resultForm = this.loginForm.value;
    if (resultForm.email && resultForm.password) {
      this.authService.login(resultForm.email, resultForm.password)
        .then(acc => {
          if (acc != null && acc.token != null && acc.id != null)
            this.router.navigate([routeConsts.overviewNav])
        })
        .catch(() => {
          this.dialog.open(ErrorDialogComponent, {
            data: 'Benutzername und Passwort stimmen nicht überein oder der Account wurde noch nicht aktiviert! Versuchen Sie es bitte erneut.'
          })
        })
    }
  }

  register(): void {
    const resultForm = this.registerForm.value;
    if (resultForm.email && resultForm.password) {
      this.authService.register(resultForm.email, resultForm.password)
        .then(ans => {
          if (ans) {
            this.dialog.open(InfoDialogComponent, {
              data: 'Sie erhalten in kürze eine Email zur Bestätigung oder werden von dem Admin hinzugefügt. ' +
                'Danach können Sie sich direkt anmelden und spielen!'
            })
          }
          else {
            this.dialog.open(ErrorDialogComponent, {
              data: 'Es gibt bereits einen Nutzer mit dieser Email! Versuchen Sie es bitte erneut.'
            });
          }
        })
        .catch(() => {
          this.dialog.open(ErrorDialogComponent, {
            data: 'Es ist ein Fehler bei der Registrierung aufgetreten! Versuchen Sie es bitte erneut.'
          });
        });
    }
  }

  resetPassword() {
    this.router.navigate([routeConsts.resetPasswordNav])
  }

}
