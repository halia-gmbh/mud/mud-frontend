import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AccountService} from '../../account/account.service';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {InfoDialogComponent} from '../../../tools/dialogs/info-dialog/info-dialog.component';
import {ErrorDialogComponent} from '../../../tools/dialogs/error-dialog/error-dialog.component';
import {routeConsts} from '../../../shared/routes';

@Component({
  selector: 'hd-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  resetForm: FormGroup

  constructor(private accService: AccountService, private dialog: MatDialog, private router: Router) { }

  ngOnInit(): void {
    this.resetForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email])
    })
  }

  reset() {
    this.accService.resetPassword(this.resetForm.value.email)
      .then(ans => {
        if (ans) {
          this.dialog.open(InfoDialogComponent, {
            data: 'Das Passwort wurde erfolgreich zurückgesetzt! Sie bekommen in kürze eine Email mit Ihrem neuen Passwort.'
          })
        }
        else {
          this.dialog.open(ErrorDialogComponent, {
            data: 'Ihre eingegebene Email konnte nicht gefunden werden! Versuchen Sie es nochmal.'
          })
        }
      }).catch(() => {
        this.dialog.open(ErrorDialogComponent, {
          data: 'Es ist ein Fehler in der aufgetreten. Versuchen Sie es nochmal.'
        })
    })
  }

  cancel() {
    this.router.navigate([routeConsts.loginNav])
  }
}
