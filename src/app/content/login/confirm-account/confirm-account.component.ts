import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AccountService} from "../../account/account.service";
import {MatDialog} from "@angular/material/dialog";
import {InfoDialogComponent} from "../../../tools/dialogs/info-dialog/info-dialog.component";
import {ErrorDialogComponent} from "../../../tools/dialogs/error-dialog/error-dialog.component";

@Component({
  selector: 'hd-confirm-account',
  templateUrl: './confirm-account.component.html',
  styleUrls: ['./confirm-account.component.scss']
})
export class ConfirmAccountComponent implements OnInit {

  confirmForm: FormGroup

  constructor(private accService: AccountService, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.confirmForm = new FormGroup({
      text: new FormControl('', [Validators.required])
    })
  }

  confirm() {
    this.accService.confirmAccount(this.confirmForm.value.text)
      .then(ans => {
        if (ans) {
          this.dialog.open(InfoDialogComponent, {
            data: 'Account erfolgreich aktiviert. Sie können sich nun in Ihren Account einloggen!'
          })
        }
        else {
          this.dialog.open(ErrorDialogComponent, {
            data: 'Ein Account mit diesem Token konnte nicht gefunden werden!'
          })
        }
      }).catch(() => {
      this.dialog.open(ErrorDialogComponent, {
        data: 'Es ist ein Fehler aufgetreten. Versuchen Sie es nochmal.'
      })
    })
  }
}
