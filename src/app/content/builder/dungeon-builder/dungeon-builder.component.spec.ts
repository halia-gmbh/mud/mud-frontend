import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DungeonBuilderComponent } from './dungeon-builder.component';

describe('DungeonBuilderComponent', () => {
  let component: DungeonBuilderComponent;
  let fixture: ComponentFixture<DungeonBuilderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DungeonBuilderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DungeonBuilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
