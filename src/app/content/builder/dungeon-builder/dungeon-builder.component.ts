import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {MatrixRoom, Room} from '../../../shared/interfaces/room/room';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DungeonBuilderService} from '../dungeon-builder.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Direction} from '../../../shared/interfaces/room/direction';
import {MatDialog} from '@angular/material/dialog';
import {ErrorDialogComponent} from '../../../tools/dialogs/error-dialog/error-dialog.component';
import {CharacterClass} from '../../../shared/interfaces/character/character-class';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {DungeonStatusEnum, Game} from '../../../shared/interfaces/game';
import {DungeonService} from '../../overview/dungeon.service';
import {CharacterRace} from '../../../shared/interfaces/character/character-race';
import {Attribute} from '../../../shared/interfaces/character/attribute-set';
import {Item} from '../../../shared/interfaces/room/item';
import {RoomEvent} from '../../../shared/interfaces/room/room-event';
import {NPC, NPCAction} from '../../../shared/interfaces/room/npc';
import {InfoDialogComponent} from '../../../tools/dialogs/info-dialog/info-dialog.component';
import {routeConsts} from '../../../shared/routes';

@Component({
  selector: 'hd-dungeon-builder',
  templateUrl: './dungeon-builder.component.html',
  styleUrls: ['./dungeon-builder.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class DungeonBuilderComponent implements OnInit {
  expandedColumn: CharacterClass | CharacterRace | Item | NPC | null;
  itemTypes = [{'CONSUMABLE': 'Konsumierbar'}, {'ARMOUR': 'Rüstung'}, {'WEAPON': 'Waffe'}, {'ACCESSOIRE': 'Accesssoire'}];

  characterColumnsToDisplay = ['name', 'description', 'buttons'];
  itemColumnsToDisplay = ['name', 'weight', 'itemType', 'buttons'];
  eventColumnsToDisplay = ['name', 'buttons'];
  npcColumnsToDisplay = ['name', 'buttons'];

  classForm: FormGroup;
  classAttributeForm: FormGroup;
  raceForm: FormGroup;
  raceAttributeForm: FormGroup;

  itemTemplateForm: FormGroup;
  itemAttributeTemplateForm: FormGroup;
  textEventTemplateForm: FormGroup;
  itemEventTemplateForm: FormGroup;
  npcTemplateForm: FormGroup;
  npcTemplateActionForm: FormGroup;

  itemForm: FormGroup;
  itemAttributeForm: FormGroup;
  textEventForm: FormGroup;
  itemEventForm: FormGroup;
  npcForm: FormGroup;
  npcActionForm: FormGroup;

  createRoomForm: FormGroup;
  editRoomForm: FormGroup;
  togglePathChange: boolean = false;

  editObjectForm: FormGroup;

  gameID: number;
  game: Game = {
    id: null,
    name: '',
    builder: 0,
    dungeonMaster: 0,
    maxActivePlayers: 0,
    dungeonStatus: DungeonStatusEnum.WIP,
    classes: [],
    races: [],
    running: false,
    dungeon: {
      startRoom: null,
      itemTemplates: [],
      npcTemplates: [],
      eventTemplates: [],
      rooms: [],
      activeCharacters: []
    },
    password: null
  };
  selectedRoom: MatrixRoom;
  selectedRoomPosition = [];
  rooms: (MatrixRoom | Direction)[][] = [
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, Direction.START, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
  ];

  constructor(private router: Router, private changeDetectorRefs: ChangeDetectorRef, private dungeonService: DungeonService, private dungeonBuilderService: DungeonBuilderService, private route: ActivatedRoute, private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.gameID = +this.route.snapshot.paramMap.get('gameID');

    this.createRoomForm = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required)
    });
    this.editRoomForm = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required)
    });

    this.classForm = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required)
    });
    this.classAttributeForm = new FormGroup({
      name: new FormControl('', Validators.required),
      value: new FormControl('', Validators.required)
    });
    this.raceForm = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required)
    });
    this.raceAttributeForm = new FormGroup({
      name: new FormControl('', Validators.required),
      value: new FormControl('', Validators.required)
    });

    this.itemTemplateForm = new FormGroup({
      name: new FormControl('', Validators.required),
      weight: new FormControl('', Validators.required),
      itemType: new FormControl('', Validators.required)
    });
    this.itemAttributeTemplateForm = new FormGroup({
      name: new FormControl('', Validators.required),
      value: new FormControl('', Validators.required)
    });

    this.npcTemplateForm = new FormGroup({
      name: new FormControl('', Validators.required)
    });
    this.npcTemplateActionForm = new FormGroup({
      name: new FormControl('', Validators.required),
      response: new FormControl('', Validators.required)
    });

    this.textEventTemplateForm = new FormGroup({
      name: new FormControl('', Validators.required),
      textResponse: new FormControl('', Validators.required)
    });
    this.itemEventTemplateForm = new FormGroup({
      name: new FormControl('', Validators.required),
      itemResponse: new FormControl('', Validators.required)
    });

    this.itemForm = new FormGroup({
      name: new FormControl('', Validators.required),
      weight: new FormControl('', Validators.required),
      itemType: new FormControl('', Validators.required)
    });
    this.itemAttributeForm = new FormGroup({
      name: new FormControl('', Validators.required),
      value: new FormControl('', Validators.required)
    });

    this.npcForm = new FormGroup({
      name: new FormControl('', Validators.required)
    });
    this.npcActionForm = new FormGroup({
      name: new FormControl('', Validators.required),
      response: new FormControl('', Validators.required)
    });

    this.textEventForm = new FormGroup({
      name: new FormControl('', Validators.required),
      textResponse: new FormControl('', Validators.required)
    });
    this.itemEventForm = new FormGroup({
      name: new FormControl('', Validators.required),
      itemResponse: new FormControl('', Validators.required)
    });


    this.dungeonService.getGame(this.gameID).toPromise().then((game: Game) => {
      this.game = game;
      if (game.dungeon.rooms.length != 0) {
        this.dungeonBuilderService.getRoomMatrix(this.gameID).toPromise().then(matrix => {
          this.rooms = matrix;
        });
      }
    });
  }

  // ROOM-OBJECTS
  // ITEM
  setEditItemAttributeValues(attribute: Attribute) {
    this.itemAttributeForm.controls.name.setValue(attribute.name);
    this.itemAttributeForm.controls.value.setValue(attribute.value);
  }

  setEditItemValues(item: Item) {
    this.itemForm.controls.name.setValue(item.name);
    this.itemForm.controls.weight.setValue(item.weight);
  }

  addItem() {
    const resultForm = this.itemForm.value;

    const item: Item = {
      id: -1,
      name: resultForm.name,
      weight: resultForm.weight,
      attributes: {attributes: []},
      itemType: resultForm.itemType
    };
    this.dungeonBuilderService.addItemToRoom(this.gameID, this.selectedRoom.room.id, item).toPromise().then(game => {
      this.game = game;
      this.itemForm.reset();
      this.dungeonBuilderService.getRoomMatrix(this.gameID).toPromise().then(matrix => {
        this.rooms = matrix;
        this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
      });
    });
  }

  editItem(item: Item) {
    const resultForm = this.itemForm.value;
    item.name = resultForm.name;
    item.weight = resultForm.weight;

    this.dungeonBuilderService.addItemToRoom(this.gameID, this.selectedRoom.room.id, item).toPromise().then(game => {
      this.game = game;
      this.itemForm.reset();
    });
  }

  addItemAttribute(item: Item) {
    const resultForm = this.itemAttributeForm.value;

    item.attributes.attributes.push({'name': resultForm.name, 'value': resultForm.value});

    this.dungeonBuilderService.addItemToRoom(this.gameID, this.selectedRoom.room.id, item).toPromise().then(game => {
      this.game = game;
      this.itemAttributeForm.reset();
    });
  }

  editItemAttribute(attribute: Attribute, item: Item) {
    const resultForm = this.itemAttributeForm.value;
    attribute.name = resultForm.name;
    attribute.value = resultForm.value;

    this.dungeonBuilderService.addItemToRoom(this.gameID, this.selectedRoom.room.id, item).toPromise().then(game => {
      this.game = game;
      this.itemAttributeForm.reset();
    });
  }

  deleteItem(itemID: number) {
    this.dungeonBuilderService.removeItemFromRoom(this.gameID, this.selectedRoom.room.id, itemID)
      .toPromise().then(game => {
      this.game = game;
      this.dungeonBuilderService.getRoomMatrix(this.gameID).toPromise().then(matrix => {
        this.rooms = matrix;
        this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
      });
    });
  }

  deleteItemAttribute(item: Item, attributeIndex: number) {
    item.attributes.attributes.splice(attributeIndex, 1);

    this.dungeonBuilderService.addItemToRoom(this.gameID, this.selectedRoom.room.id, item).toPromise().then(game => {
      this.game = game;
      this.itemTemplateForm.reset();
    });
  }

  // ROOMEVENT
  setEditTextEventValues(roomEvent: RoomEvent) {
    this.textEventForm.controls.name.setValue(roomEvent.name);
    this.textEventForm.controls.textResponse.setValue(roomEvent.textResponse);
  }

  setEditItemEventValues(roomEvent: RoomEvent) {
    this.itemEventForm.controls.name.setValue(roomEvent.name);
  }

  addTextEvent() {
    const resultForm = this.textEventForm.value;

    const roomEvent: RoomEvent = {
      id: -1,
      name: resultForm.name,
      textResponse: resultForm.textResponse,
      itemResponse: null
    };
    this.dungeonBuilderService.addRoomEventToRoom(this.gameID, this.selectedRoom.room.id, roomEvent).toPromise().then(game => {
      this.game = game;
      this.textEventForm.reset();
      this.dungeonBuilderService.getRoomMatrix(this.gameID).toPromise().then(matrix => {
        this.rooms = matrix;
        this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
      });
    });
  }

  addItemEvent() {
    const resultForm = this.itemEventForm.value;

    const roomEvent: RoomEvent = {
      id: -1,
      name: resultForm.name,
      textResponse: null,
      itemResponse: resultForm.itemResponse
    };
    this.dungeonBuilderService.addRoomEventToRoom(this.gameID, this.selectedRoom.room.id, roomEvent).toPromise().then(game => {
      this.game = game;
      this.itemEventForm.reset();
      this.dungeonBuilderService.getRoomMatrix(this.gameID).toPromise().then(matrix => {
        this.rooms = matrix;
        this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
      });
    });
  }

  editTextEvent(event: RoomEvent) {
    const resultForm = this.textEventForm.value;
    event.name = resultForm.name;
    event.textResponse = resultForm.textResponse;

    this.dungeonBuilderService.addRoomEventToRoom(this.gameID, this.selectedRoom.room.id, event).toPromise().then(game => {
      this.game = game;
      this.textEventForm.reset();
    });
  }

  editItemEvent(event: RoomEvent) {
    const resultForm = this.itemEventForm.value;
    event.name = resultForm.name;
    event.itemResponse = resultForm.itemResponse;

    this.dungeonBuilderService.addRoomEventToRoom(this.gameID, this.selectedRoom.room.id, event).toPromise().then(game => {
      this.game = game;
      this.itemEventForm.reset();
    });
  }

  deleteRoomEvent(eventID: number) {
    this.dungeonBuilderService.removeRoomEventFromRoom(this.gameID, this.selectedRoom.room.id, eventID)
      .toPromise().then(game => {
      this.game = game;
      this.dungeonBuilderService.getRoomMatrix(this.gameID).toPromise().then(matrix => {
        this.rooms = matrix;
        this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
      });
    });
  }

  // NPC
  setEditNpcValues(npc: NPC) {
    this.npcForm.controls.name.setValue(npc.name);
  }

  setEditNpcActionValues(action: NPCAction) {
    this.npcActionForm.controls.name.setValue(action.name);
    this.npcActionForm.controls.response.setValue(action.response);
  }

  addNpc() {
    const resultForm = this.npcForm.value;

    const npc: NPC = {
      id: -1,
      name: resultForm.name,
      actions: []
    };
    this.dungeonBuilderService.addNpcToRoom(this.gameID, this.selectedRoom.room.id, npc)
      .toPromise().then(game => {
      this.game = game;
      this.npcTemplateForm.reset();
      this.dungeonBuilderService.getRoomMatrix(this.gameID).toPromise().then(matrix => {
        this.rooms = matrix;
        this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
      });
    });
  }

  editNpc(npc: NPC) {
    const resultForm = this.npcForm.value;
    npc.name = resultForm.name;

    this.dungeonBuilderService.addNpcToRoom(this.gameID, this.selectedRoom.room.id, npc)
      .toPromise().then(game => {
      this.game = game;
      this.npcForm.reset();
    });
  }

  deleteNpc(npcID: number) {
    this.dungeonBuilderService.removeNpcFromRoom(this.gameID, this.selectedRoom.room.id, npcID)
      .toPromise().then(game => {
      this.game = game;
      this.dungeonBuilderService.getRoomMatrix(this.gameID).toPromise().then(matrix => {
        this.rooms = matrix;
        this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
      });
    });
  }

  addNpcAction(npc: NPC) {
    const resultForm = this.npcActionForm.value;

    npc.actions.push({name: resultForm.name, response: resultForm.response});

    this.dungeonBuilderService.addNpcToRoom(this.gameID, this.selectedRoom.room.id, npc)
      .toPromise().then(game => {
      this.game = game;
      this.npcActionForm.reset();
    });
  }

  editNpcAction(action: NPCAction, npc: NPC) {
    const resultForm = this.npcActionForm.value;
    action.name = resultForm.name;
    action.response = resultForm.response;

    this.dungeonBuilderService.addNpcToRoom(this.gameID, this.selectedRoom.room.id, npc)
      .toPromise().then(game => {
      this.game = game;
      this.npcActionForm.reset();
    });
  }

  deleteNpcAction(npc: NPC, actionIndex: number) {
    npc.actions.splice(actionIndex, 1);
    this.dungeonBuilderService.addNpcToRoom(this.gameID, this.selectedRoom.room.id, npc)
      .toPromise().then(game => {
      this.game = game;
    });
  }


  // OBJECT-TEMPLATES
  // ITEM
  setEditItemAttributeTemplateValues(attribute: Attribute) {
    this.itemAttributeTemplateForm.controls.name.setValue(attribute.name);
    this.itemAttributeTemplateForm.controls.value.setValue(attribute.value);
  }

  setEditItemTemplateValues(item: Item) {
    this.itemTemplateForm.controls.name.setValue(item.name);
    this.itemTemplateForm.controls.weight.setValue(item.weight);
  }

  addItemTemplate() {
    const resultForm = this.itemTemplateForm.value;

    const item: Item = {
      id: -1,
      name: resultForm.name,
      weight: resultForm.weight,
      attributes: {attributes: []},
      itemType: resultForm.itemType
    };
    this.dungeonBuilderService.addItemTemplate(this.gameID, item).toPromise().then(game => {
      this.game = game;
      this.itemTemplateForm.reset();
    });
  }

  editItemTemplate(item: Item) {
    const resultForm = this.itemTemplateForm.value;
    item.name = resultForm.name;
    item.weight = resultForm.weight;

    this.dungeonBuilderService.addItemTemplate(this.gameID, item).toPromise().then(game => {
      this.game = game;
      this.itemTemplateForm.reset();
    });
  }

  addItemAttributeTemplate(item: Item) {
    const resultForm = this.itemAttributeTemplateForm.value;

    item.attributes.attributes.push({'name': resultForm.name, 'value': resultForm.value});

    this.dungeonBuilderService.addItemTemplate(this.gameID, item).toPromise().then(game => {
      this.game = game;
      this.itemAttributeTemplateForm.reset();
    });
  }

  editItemAttributeTemplate(attribute: Attribute, item: Item) {
    const resultForm = this.itemAttributeTemplateForm.value;
    attribute.name = resultForm.name;
    attribute.value = resultForm.value;

    this.dungeonBuilderService.addItemTemplate(this.gameID, item).toPromise().then(game => {
      this.game = game;
      this.itemAttributeTemplateForm.reset();
    });
  }

  deleteItemTemplate(itemID: number) {
    this.dungeonBuilderService.removeItemTemplate(this.gameID, itemID)
      .toPromise().then(game => this.game = game);
  }

  deleteItemAttributeTemplate(item: Item, attributeIndex: number) {
    item.attributes.attributes.splice(attributeIndex, 1);
    this.dungeonBuilderService.addItemTemplate(this.gameID, item).toPromise().then(game => {
      this.game = game;
    });
  }

  addItemFromTemplate(item: Item) {
    if (this.selectedRoom && this.selectedRoom.room) {
      const roomItem: Item = {
        ...item,
        id: -1
      };
      this.dungeonBuilderService.addItemToRoom(this.gameID, this.selectedRoom.room.id, roomItem).toPromise().then(game => {
        this.game = game;
        this.dungeonBuilderService.getRoomMatrix(this.gameID).toPromise().then(matrix => {
          this.rooms = matrix;
          this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
        });
      });
    } else {
      this.dialog.open(InfoDialogComponent, {
        data: 'Es muss ein Raum ausgewählt sein!'
      });
    }
  }


  // ROOMEVENT
  setEditTextEventTemplateValues(roomEvent: RoomEvent) {
    this.textEventTemplateForm.controls.name.setValue(roomEvent.name);
    this.textEventTemplateForm.controls.textResponse.setValue(roomEvent.textResponse);
  }

  setEditItemEventTemplateValues(roomEvent: RoomEvent) {
    this.itemEventTemplateForm.controls.name.setValue(roomEvent.name);
  }

  addTextEventTemplate() {
    const resultForm = this.textEventTemplateForm.value;

    const roomEvent: RoomEvent = {
      id: -1,
      name: resultForm.name,
      textResponse: resultForm.textResponse,
      itemResponse: null
    };
    this.dungeonBuilderService.addRoomEventTemplate(this.gameID, roomEvent).toPromise().then(game => {
      this.game = game;
      this.textEventTemplateForm.reset();
    });
  }

  addItemEventTemplate() {
    const resultForm = this.itemEventTemplateForm.value;

    const roomEvent: RoomEvent = {
      id: -1,
      name: resultForm.name,
      textResponse: null,
      itemResponse: resultForm.itemResponse
    };
    this.dungeonBuilderService.addRoomEventTemplate(this.gameID, roomEvent).toPromise().then(game => {
      this.game = game;
      this.itemEventTemplateForm.reset();
    });
  }

  editTextEventTemplate(event: RoomEvent) {
    const resultForm = this.textEventTemplateForm.value;
    event.name = resultForm.name;
    event.textResponse = resultForm.textResponse;

    this.dungeonBuilderService.addRoomEventTemplate(this.gameID, event).toPromise().then(game => {
      this.game = game;
      this.textEventTemplateForm.reset();
    });
  }

  editItemEventTemplate(event: RoomEvent) {
    const resultForm = this.itemEventTemplateForm.value;
    event.name = resultForm.name;
    event.itemResponse = resultForm.itemResponse;

    this.dungeonBuilderService.addRoomEventTemplate(this.gameID, event).toPromise().then(game => {
      this.game = game;
      this.itemEventTemplateForm.reset();
    });
  }

  deleteRoomEventTemplate(eventID: number) {
    this.dungeonBuilderService.removeRoomEventTemplate(this.gameID, eventID)
      .toPromise().then(game => this.game = game);
  }

  addRoomEventFromTemplate(roomEvent: RoomEvent) {
    if (this.selectedRoom && this.selectedRoom.room) {
      const event: RoomEvent = {
        ...roomEvent,
        id: -1
      };
      this.dungeonBuilderService.addRoomEventToRoom(this.gameID, this.selectedRoom.room.id, event)
        .toPromise().then(game => {
        this.game = game;
        this.dungeonBuilderService.getRoomMatrix(this.gameID).toPromise().then(matrix => {
          this.rooms = matrix;
          this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
        });
      });
    } else {
      this.dialog.open(InfoDialogComponent, {
        data: 'Es muss ein Raum ausgewählt sein!'
      });
    }
  }


  // NPC
  setEditNpcTemplateValues(npc: NPC) {
    this.npcTemplateForm.controls.name.setValue(npc.name);
  }

  setEditNpcActionTemplateValues(action: NPCAction) {
    this.npcTemplateActionForm.controls.name.setValue(action.name);
    this.npcTemplateActionForm.controls.response.setValue(action.response);
  }

  addNpcTemplate() {
    const resultForm = this.npcTemplateForm.value;

    const npc: NPC = {
      id: -1,
      name: resultForm.name,
      actions: []
    };
    this.dungeonBuilderService.addNpcTemplate(this.gameID, npc)
      .toPromise().then(game => {
      this.game = game;
      this.npcTemplateForm.reset();
    });
  }

  editNpcTemplate(npc: NPC) {
    const resultForm = this.npcTemplateForm.value;
    npc.name = resultForm.name;

    this.dungeonBuilderService.addNpcTemplate(this.gameID, npc)
      .toPromise().then(game => {
      this.game = game;
      this.npcTemplateForm.reset();
    });
  }

  deleteNpcTemplate(npcID: number) {
    this.dungeonBuilderService.removeNpcTemplate(this.gameID, npcID)
      .toPromise().then(game => this.game = game);
  }

  addNpcActionTemplate(npc: NPC) {
    const resultForm = this.npcTemplateActionForm.value;

    npc.actions.push({name: resultForm.name, response: resultForm.response});

    this.dungeonBuilderService.addNpcTemplate(this.gameID, npc)
      .toPromise().then(game => {
      this.game = game;
      this.npcTemplateActionForm.reset();
    });
  }

  editNpcActionTemplate(action: NPCAction, npc: NPC) {
    const resultForm = this.npcTemplateActionForm.value;
    action.name = resultForm.name;
    action.response = resultForm.response;

    this.dungeonBuilderService.addNpcTemplate(this.gameID, npc)
      .toPromise().then(game => {
      this.game = game;
      this.npcTemplateActionForm.reset();
    });
  }

  deleteNpcActionTemplate(npc: NPC, actionIndex: number) {
    npc.actions.splice(actionIndex, 1);
    this.dungeonBuilderService.addNpcTemplate(this.gameID, npc)
      .toPromise().then(game => {
      this.game = game;
    });
  }

  addNpcFromTemplate(npc: NPC) {
    if (this.selectedRoom && this.selectedRoom.room) {
      const roomNpc: NPC = {
        ...npc,
        id: -1
      };
      this.dungeonBuilderService.addNpcToRoom(this.gameID, this.selectedRoom.room.id, roomNpc)
        .toPromise().then(game => {
        this.game = game;
        this.dungeonBuilderService.getRoomMatrix(this.gameID).toPromise().then(matrix => {
          this.rooms = matrix;
          this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
        });
      });
    } else {
      this.dialog.open(InfoDialogComponent, {
        data: 'Es muss ein Raum ausgewählt sein!'
      });
    }
  }


  // CHARACTER-OPTIONS
  // CLASS
  setEditClassAttributeValues(attribute: Attribute) {
    this.classAttributeForm.controls.name.setValue(attribute.name);
    this.classAttributeForm.controls.value.setValue(attribute.value);
  }

  setEditClassValues(characterClass: CharacterClass) {
    this.classForm.controls.name.setValue(characterClass.name);
    this.classForm.controls.description.setValue(characterClass.description);
  }

  addClass() {
    const resultForm = this.classForm.value;

    const characterClass: CharacterClass = {
      id: -1,
      name: resultForm.name,
      description: resultForm.description,
      attributes: {attributes: []}
    };
    this.dungeonBuilderService.addCharacterClass(this.gameID, characterClass).toPromise().then(game => {
      this.game = game;
      this.classForm.reset();
    });
  }

  editClass(characterClass: CharacterClass) {
    const resultForm = this.classForm.value;
    characterClass.name = resultForm.name;
    characterClass.description = resultForm.description;

    this.dungeonBuilderService.addCharacterClass(this.gameID, characterClass).toPromise().then(game => {
      this.game = game;
      this.classForm.reset();
    });
  }

  addClassAttribute(characterClass: CharacterClass) {
    const resultForm = this.classAttributeForm.value;
    characterClass.attributes.attributes.push({'name': resultForm.name, 'value': resultForm.value});

    this.dungeonBuilderService.addCharacterClass(this.gameID, characterClass).toPromise().then(game => {
      this.game = game;
      this.classAttributeForm.reset();
    });
  }

  editClassAttribute(attribute: Attribute, characterClass: CharacterClass) {
    const resultForm = this.classAttributeForm.value;
    attribute.name = resultForm.name;
    attribute.value = resultForm.value;

    this.dungeonBuilderService.addCharacterClass(this.gameID, characterClass).toPromise().then(game => {
      this.game = game;
      this.classAttributeForm.reset();
    });
  }

  deleteClass(classID: number) {
    this.dungeonBuilderService.deleteCharacterClass(this.gameID, classID)
      .toPromise().then(game => {
      this.game = game;
    });
  }

  deleteClassAttribute(characterClass: CharacterClass, attributeIndex: number) {
    characterClass.attributes.attributes.splice(attributeIndex, 1);
    this.dungeonBuilderService.addCharacterClass(this.gameID, characterClass).toPromise().then(game => {
      this.game = game;
    });
  }

  // RACE
  setEditRaceAttributeValues(attribute: Attribute) {
    this.raceAttributeForm.controls.name.setValue(attribute.name);
    this.raceAttributeForm.controls.value.setValue(attribute.value);
  }

  setEditRaceValues(characterClass: CharacterClass) {
    this.raceForm.controls.name.setValue(characterClass.name);
    this.raceForm.controls.description.setValue(characterClass.description);
  }

  addRace() {
    const resultForm = this.raceForm.value;

    const characterRace: CharacterRace = {
      id: -1,
      name: resultForm.name,
      description: resultForm.description,
      attributes: {attributes: []}
    };
    this.dungeonBuilderService.addCharacterRace(this.gameID, characterRace).toPromise().then(game => {
      this.game = game;
      this.raceForm.reset();
    });
  }

  addRaceAttribute(characterRace: CharacterRace) {
    const resultForm = this.raceAttributeForm.value;
    characterRace.attributes.attributes.push({'name': resultForm.name, 'value': resultForm.value});

    this.dungeonBuilderService.addCharacterRace(this.gameID, characterRace).toPromise().then(game => {
      this.game = game;
      this.raceAttributeForm.reset();
    });
  }

  editRace(characterRace: CharacterRace) {
    const resultForm = this.raceForm.value;
    characterRace.name = resultForm.name;
    characterRace.description = resultForm.description;

    this.dungeonBuilderService.addCharacterRace(this.gameID, characterRace).toPromise().then(game => {
      this.game = game;
      this.raceForm.reset();
    });
  }

  editRaceAttribute(attribute: Attribute, characterRace: CharacterRace) {
    const resultForm = this.raceAttributeForm.value;
    attribute.name = resultForm.name;
    attribute.value = resultForm.value;

    this.dungeonBuilderService.addCharacterRace(this.gameID, characterRace).toPromise().then(game => {
      this.game = game;
      this.raceAttributeForm.reset();
    });
  }

  deleteRace(raceID: number) {
    this.dungeonBuilderService.deleteCharacterRace(this.gameID, raceID)
      .toPromise().then(game => {
      this.game = game;
    });
  }

  deleteRaceAttribute(characterRace: CharacterRace, attributeIndex: number) {
    characterRace.attributes.attributes.splice(attributeIndex, 1);
    this.dungeonBuilderService.addCharacterRace(this.gameID, characterRace).toPromise().then(game => {
      this.game = game;
    });
  }


  // ROOMS
  createRoom(x: number, y: number) {
    const roomInfo = this.createRoomForm.value;

    if (this.rooms[x][y] == Direction.START) {
      this.dungeonBuilderService.createRoom(this.gameID, roomInfo.name, roomInfo.description)
        .toPromise().then(room => {
        if (room) {
          this.rooms[x][y] = {
            room: room,
            north: false,
            east: false,
            south: false,
            west: false,
            startRoom: true
          };
          this.selectRoom(x, y);
          this.createRoomForm.reset();
        }
      }, () => {
        this.dialog.open(ErrorDialogComponent, {
          data: 'Es ist ein Fehler bei der Raumerstellung aufgetreten! Versuchen Sie es bitte erneut.'
        });
      });
    } else {
      const direction: Direction = this.rooms[x][y] as number;
      let room1 = this.selectedRoom;

      this.dungeonBuilderService.createRoom(this.gameID, roomInfo.name, roomInfo.description)
        .toPromise().then((room: Room) => {
        this.dungeonBuilderService.connectRooms(this.gameID, room1.room.id, room.id, direction)
          .toPromise().then((rooms: MatrixRoom[][]) => {
          if (rooms) {
            this.rooms = rooms;
            this.selectRoom(x, y);
            this.createRoomForm.reset();
          }
        });
      }, () => {
        this.dialog.open(ErrorDialogComponent, {
          data: 'Es ist ein Fehler bei der Raumerstellung aufgetreten! Versuchen Sie es bitte erneut.'
        });
      });
    }
  }

  selectRoom(x: number, y: number) {
    this.selectedRoom = this.rooms[x][y] as MatrixRoom;
    this.selectedRoomPosition[0] = x;
    this.selectedRoomPosition[1] = y;
    this.resetRoomAddButtons();

    if (x - 1 >= 0 && this.rooms[x - 1][y] == null) {
      this.rooms[x - 1][y] = Direction.WEST;
    }
    if (x + 1 < this.rooms.length && this.rooms[x + 1][y] == null) {
      this.rooms[x + 1][y] = Direction.EAST;
    }
    if (y - 1 >= 0 && this.rooms[x][y - 1] == null) {
      this.rooms[x][y - 1] = Direction.NORTH;
    }
    if (y + 1 < this.rooms[x].length && this.rooms[x][y + 1] == null) {
      this.rooms[x][y + 1] = Direction.SOUTH;
    }
  }

  selectRoomPathChange(x: number, y: number) {
    this.selectedRoom = this.rooms[x][y] as MatrixRoom;
    this.selectedRoomPosition[0] = x;
    this.selectedRoomPosition[1] = y;
    this.resetPathButtons();

    const west: MatrixRoom = this.rooms[x - 1][y] as MatrixRoom;
    const east: MatrixRoom = this.rooms[x + 1][y] as MatrixRoom;
    const north: MatrixRoom = this.rooms[x][y - 1] as MatrixRoom;
    const south: MatrixRoom = this.rooms[x][y + 1] as MatrixRoom;

    if (x - 1 >= 0 && west && west.room) {
      west.pathChangeDirection = Direction.WEST;
      west.deletePath = this.selectedRoom.west == true;
    }
    if (x + 1 < this.rooms.length && east && east.room) {
      east.pathChangeDirection = Direction.EAST;
      east.deletePath = this.selectedRoom.east == true;
    }
    if (y - 1 >= 0 && north && north.room) {
      north.pathChangeDirection = Direction.NORTH;
      north.deletePath = this.selectedRoom.north == true;
    }
    if (y + 1 < this.rooms[x].length && south && south.room) {
      south.pathChangeDirection = Direction.SOUTH;
      south.deletePath = this.selectedRoom.south == true;
    }
  }

  removePathway(x: number, y: number) {
    const room2: MatrixRoom = this.rooms[x][y] as MatrixRoom;
    this.dungeonBuilderService.removePathway(this.gameID, this.selectedRoom.room.id, room2.room.id).toPromise()
      .then(rooms => {
        if (rooms) {
          this.rooms = rooms;
          this.selectRoomPathChange(x, y);
        } else {
          this.dialog.open(InfoDialogComponent, {
            data: 'Dieser Pfad kann nicht gelöscht werden. Ein Raum muss immer zum Startraum verbunden sein!'
          });
        }
      });
  }

  addPathway(x: number, y: number) {
    const room2: MatrixRoom = this.rooms[x][y] as MatrixRoom;
    this.dungeonBuilderService.connectRooms(this.gameID, this.selectedRoom.room.id, room2.room.id, room2.pathChangeDirection)
      .toPromise()
      .then(rooms => {
        this.rooms = rooms;
        this.selectRoomPathChange(x, y);
      });

  }

  switchPathChange() {
    if (this.togglePathChange == true) {
      if (this.rooms[7][7] == null) {
        this.rooms = [
          [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
          [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
          [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
          [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
          [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
          [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
          [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
          [null, null, null, null, null, null, null, Direction.START, null, null, null, null, null, null, null],
          [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
          [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
          [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
          [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
          [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
          [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
          [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
        ];
      } else {
        this.resetPathButtons();
        this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
      }
    } else {
      this.resetRoomAddButtons();
      this.selectRoomPathChange(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
    }
  }

  unselectRoom() {
    this.resetRoomAddButtons();
    this.resetPathButtons();
    this.selectedRoom = null;
    this.selectedRoomPosition = [];
  }

  deleteRoom() {
    if (this.selectedRoom) {
      if (this.selectedRoom.startRoom) {
        this.dialog.open(ErrorDialogComponent, {
          data: 'Der Startraum kann nicht gelöscht werden! Sie können Ihn aber bearbeiten.'
        });
      } else {
        this.dungeonBuilderService.deleteRoom(this.gameID, this.selectedRoom.room.id)
          .toPromise().then(rooms => {
          if (rooms) {
            this.rooms = rooms;
            this.selectedRoom = null;
            this.resetRoomAddButtons();
          } else {
            this.dialog.open(InfoDialogComponent, {
              data: 'Dieser Raum kann nicht gelöscht werden. Alle Räume müssen mit dem Startraum verbunden sein!'
            });
          }
        })
          .catch(() => {
            this.dialog.open(ErrorDialogComponent, {
              data: 'Es ist ein Fehler beim Löschen des Raumes aufgetreten! Versuchen Sie es bitte erneut.'
            });
          });
      }
    }
  }

  editRoom() {
    const resultForm = this.editRoomForm.value;

    this.dungeonBuilderService.editRoom(this.gameID, this.selectedRoom.room.id, resultForm.name, resultForm.description)
      .toPromise().then(rooms => {
      this.rooms = rooms;
      this.selectRoom(this.selectedRoomPosition[0], this.selectedRoomPosition[1]);
      this.editRoomForm.reset();
    });
  }

  private resetRoomAddButtons() {
    for (let x = 0; x < this.rooms.length; x++) {
      for (let y = 0; y < this.rooms[x].length; y++) {
        if (this.rooms[x][y] >= 1) {
          this.rooms[x][y] = null;
        }
      }
    }
  }

  private resetPathButtons() {
    for (let x = 0; x < this.rooms.length; x++) {
      for (let y = 0; y < this.rooms[x].length; y++) {
        if ((this.rooms[x][y] as MatrixRoom)?.deletePath != null) {
          (this.rooms[x][y] as MatrixRoom).deletePath = null;
        }
        if ((this.rooms[x][y] as MatrixRoom)?.pathChangeDirection) {
          (this.rooms[x][y] as MatrixRoom).pathChangeDirection = null;
        }
      }
    }
  }

  setEditRoomValues() {
    this.editRoomForm.controls.name.setValue(this.selectedRoom.room.name);
    this.editRoomForm.controls.description.setValue(this.selectedRoom.room.description);
  }

  convertItemTypeString(type: string) {
    switch (type) {
      case 'CONSUMABLE':
        return 'Konsumierbar';
      case 'ARMOUR':
        return 'Rüstung';
      case 'WEAPON':
        return 'Waffe';
      case 'ACCESSOIRE':
        return 'Accessoire';
    }
  }

  save() {
    this.dungeonBuilderService.saveDungeon(this.gameID).toPromise()
      .then(() => {
        this.router.navigate([routeConsts.overviewNav]);
      })
      .catch(() => {
        this.dialog.open(ErrorDialogComponent, {
          data: 'Es ist ein Fehler beim Speichern des Dungeons aufgetreten.'
        });
      });
  }

  publish() {
    if (this.game.dungeon.startRoom != null && this.game.races.length > 0 && this.game.classes.length > 0) {
      this.dungeonBuilderService.publishDungeon(this.gameID).toPromise()
        .then(ans => {
          if (ans) {
            this.router.navigate([routeConsts.overviewNav]);
          }
        }).catch(() => {
        this.dialog.open(ErrorDialogComponent, {
          data: 'Es ist ein Fehler beim Veröffentlichen des Dungeons aufgetreten.'
        });
      });
    } else {
      this.dialog.open(InfoDialogComponent, {
        data: 'Es muss ein Raum, eine Klasse und eine Rasse erstellt sein um ein Dungeon zu veröffentlichen.'
      });
    }
  }

  delete() {
    this.dungeonBuilderService.deleteDungeon(this.gameID).toPromise()
      .then(ans => {
        if (ans > 0) {
          this.router.navigate([routeConsts.overviewNav]);
        }
      }).catch(() => {
      this.dialog.open(ErrorDialogComponent, {
        data: 'Es ist ein Fehler beim Löschen des Dungeons aufgetreten.'
      });
    });
  }
}
