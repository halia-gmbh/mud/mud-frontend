import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {api} from '../../shared/api';
import {Direction} from '../../shared/interfaces/room/direction';
import {MatrixRoom, Room} from '../../shared/interfaces/room/room';
import {CharacterClass} from '../../shared/interfaces/character/character-class';
import {Game} from '../../shared/interfaces/game';
import {CharacterRace} from '../../shared/interfaces/character/character-race';
import {RoomEvent} from '../../shared/interfaces/room/room-event';
import {NPC} from '../../shared/interfaces/room/npc';
import {Item} from '../../shared/interfaces/room/item';

@Injectable({
  providedIn: 'root'
})
export class DungeonBuilderService {

  constructor(private httpClient: HttpClient) {
  }

  saveDungeon(gameID: number) {
    return this.httpClient
      .post<boolean>(`${environment.baseUrl}${api.saveDungeon_GameID}${gameID}`, '');
  }

  publishDungeon(gameID: number) {
    return this.httpClient
      .post<boolean>(`${environment.baseUrl}${api.publishDungeon_GameID}${gameID}`, '');
  }

  deleteDungeon(gameID: number) {
    return this.httpClient
      .post<number>(`${environment.baseUrl}${api.deleteDungeon_GameID}${gameID}`, '');
  }

  // ROOM
  createRoom(gameID: number, name: string, description: string) {
    return this.httpClient
      .post<Room>(`${environment.baseUrl}${api.createRoom_GameID_Name_Description}${gameID}/${name}/${description}`, '');
  }

  connectRooms(gameID: number, room1ID: number, room2ID: number, direction: Direction) {
    return this.httpClient.post<MatrixRoom[][]>(`${environment.baseUrl}${api.connectRooms_GameID_From_To_Direction}${gameID}/${room1ID}/${room2ID}/${direction}`, '');
  }

  removePathway(gameID: number, room1ID: number, room2ID: number) {
    return this.httpClient.post<MatrixRoom[][]>(`${environment.baseUrl}${api.removePathway_GameID_From_To}${gameID}/${room1ID}/${room2ID}`, '');
  }

  getRoomMatrix(gameID: number) {
    return this.httpClient.get<MatrixRoom[][]>(`${environment.baseUrl}${api.getMatrix_GameID}${gameID}`);
  }

  deleteRoom(gameID: number, roomID: number) {
    return this.httpClient.post<MatrixRoom[][]>(`${environment.baseUrl}${api.deleteRoom_GameID_RoomID}${gameID}/${roomID}`, '');
  }

  editRoom(gameID: number, roomID: number, name: string, description: string) {
    return this.httpClient.post<MatrixRoom[][]>(`${environment.baseUrl}${api.editRoom_GameID_RoomID_Name_Description}${gameID}/${roomID}/${name}/${description}`, '');
  }

  // CHARACTER
  addCharacterClass(gameID: number, characterClass: CharacterClass) {
    return this.httpClient.post<Game>(`${environment.baseUrl}${api.addCharacterClass_GameID}${gameID}`, characterClass);
  }

  deleteCharacterClass(gameID: number, classID: number) {
    return this.httpClient.post<Game>(`${environment.baseUrl}${api.removeCharacterClass_GameID_ClassID}${gameID}/${classID}`, '');
  }

  addCharacterRace(gameID: number, characterRace: CharacterRace) {
    return this.httpClient.post<Game>(`${environment.baseUrl}${api.addCharacterRace_GameID}${gameID}`, characterRace);
  }

  deleteCharacterRace(gameID: number, raceID: number) {
    return this.httpClient.post<Game>(`${environment.baseUrl}${api.removeCharacterRace_GameID_Race_ID}${gameID}/${raceID}`, '');
  }

  // OBJECTS
  addItemTemplate(gameID: number, item: Item) {
    return this.httpClient.post<Game>(`${environment.baseUrl}${api.addItemTemplate_GameID}${gameID}`, item);
  }

  addRoomEventTemplate(gameID: number, roomEvent: RoomEvent) {
    return this.httpClient.post<Game>(`${environment.baseUrl}${api.addRoomEventTemplate_GameID}${gameID}`, roomEvent);
  }

  addNpcTemplate(gameID: number, npc: NPC) {
    return this.httpClient.post<Game>(`${environment.baseUrl}${api.addNpcTemplate_GameID}${gameID}`, npc);
  }

  removeItemTemplate(gameID: number, itemID: number) {
    return this.httpClient.post<Game>(`${environment.baseUrl}${api.removeItemTemplate_GameID_ItemID}${gameID}/${itemID}`, '');
  }

  removeRoomEventTemplate(gameID: number, eventID: number) {
    return this.httpClient.post<Game>(`${environment.baseUrl}${api.removeRoomEventTemplate_GameID_RoomEventID}${gameID}/${eventID}`, '');
  }

  removeNpcTemplate(gameID: number, npcID: number) {
    return this.httpClient.post<Game>(`${environment.baseUrl}${api.removeNpcTemplate_GameID_NpcID}${gameID}/${npcID}`, '');
  }


  addItemToRoom(gameID: number, roomID: number, item: Item) {
    return this.httpClient.post<Game>(`${environment.baseUrl}${api.addItemToRoom_GameID_RoomID}${gameID}/${roomID}`, item);
  }

  addRoomEventToRoom(gameID: number, roomID: number, roomEvent: RoomEvent) {
    return this.httpClient.post<Game>(`${environment.baseUrl}${api.addRoomEventToRoom_GameID_RoomID}${gameID}/${roomID}`, roomEvent);
  }

  addNpcToRoom(gameID: number, roomID: number, npc: NPC) {
    return this.httpClient.post<Game>(`${environment.baseUrl}${api.addNpcToRoom_GameID_RoomID}${gameID}/${roomID}`, npc);
  }

  removeItemFromRoom(gameID: number, roomID: number, itemID: number) {
    return this.httpClient.post<Game>(`${environment.baseUrl}${api.removeItemFromRoom_GameID_RoomID_ItemID}${gameID}/${roomID}/${itemID}`, '');
  }

  removeRoomEventFromRoom(gameID: number, roomID: number, eventID: number) {
    return this.httpClient.post<Game>(`${environment.baseUrl}${api.removeRoomEventFromRoom_GameID_RoomID_RoomEventID}${gameID}/${roomID}/${eventID}`, '');
  }

  removeNpcFromRoom(gameID: number, roomID: number, npcID: number) {
    return this.httpClient.post<Game>(`${environment.baseUrl}${api.removeNpcFromRoom_GameID_RoomID_NpcID}${gameID}/${roomID}/${npcID}`, '');
  }
}
