import { TestBed } from '@angular/core/testing';

import { DungeonBuilderService } from './dungeon-builder.service';

describe('DungeonBuilderService', () => {
  let service: DungeonBuilderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DungeonBuilderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
