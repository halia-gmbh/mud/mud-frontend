import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../../auth/auth.service";
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {ErrorDialogComponent} from "../../../tools/dialogs/error-dialog/error-dialog.component";
import {AccountService} from "../account.service";
import {routeConsts} from "../../../shared/routes";

@Component({
  selector: 'hd-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.scss']
})
export class AccountSettingsComponent implements OnInit {
  newPasswordForm: FormGroup;
  newEmailForm: FormGroup;

  constructor(private accountService: AccountService,
              private router: Router,
              private dialog: MatDialog,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.newPasswordForm = new FormGroup({
      password: new FormControl('', [Validators.required]),
      password2: new FormControl('', Validators.required)
    });
    this.newEmailForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
    });
  }

  newEmail() {
    const resultForm = this.newEmailForm.value;
    const userID = sessionStorage.getItem('userID')
    this.accountService.newEmail(resultForm.email, userID)
      .then(acc => {
        if (acc != null) {
          this.authService.logout(userID).then(e => {
            this.router.navigate([routeConsts.loginNav])
          });
        }})
      .catch((e) => {
        console.log(e)
        this.dialog.open(ErrorDialogComponent, {
          data: 'Versuchen Sie es bitte erneut.'
        })
      })
  }

  newPassword() {
    const resultForm = this.newPasswordForm.value;
    const userID = sessionStorage.getItem('userID')
    if (resultForm.password == resultForm.password2){
      this.accountService.newPassword(resultForm.password, userID)
        .then(acc => {
          if (acc != null)
            this.authService.logout(userID).then( e => {
              this.router.navigate([routeConsts.loginNav])
            });
        })
        .catch(() => {
          this.dialog.open(ErrorDialogComponent, {
            data: 'Versuchen Sie es bitte erneut.'
          })
        })
    }
    else{
      this.dialog.open(ErrorDialogComponent, {
        data: 'Passwörter stimmen nicht überein. Versuchen Sie es bitte erneut.'
      })
    }

  }
}
