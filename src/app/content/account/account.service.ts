import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {api} from '../../shared/api';
import {Room} from "../../shared/interfaces/room/room";

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private httpClient: HttpClient) { }

  resetPassword(email: string) {
    return this.httpClient.post<boolean>(environment.baseUrl+api.resetPassword, email).toPromise()
  }

  confirmAccount(token: string) {
    return this.httpClient.post<boolean>(environment.baseUrl+api.confirmAccount, token).toPromise()
  }
  newEmail(email: string, userID: string ):Promise<boolean>{
    return this.httpClient.post<boolean>(`${environment.baseUrl}${api.newEmail_UserID_Email}${userID}/${email}`, '').toPromise()
  }

  newPassword(password: string, userID: string):Promise<boolean>{
    return this.httpClient.post<boolean>(`${environment.baseUrl}${api.newPassword_UserID_Password}${userID}/${password}`, '').toPromise()
  }
}
