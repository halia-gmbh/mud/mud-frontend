import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {race} from "rxjs";
import {CharacterRace} from "../../../shared/interfaces/character/character-race";
import {CharacterClass} from "../../../shared/interfaces/character/character-class";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {InfoDialogComponent} from "../../../tools/dialogs/info-dialog/info-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {DungeonService} from "../dungeon.service";
import {Character} from "../../../shared/interfaces/character/character";
import {ActivatedRoute, Router} from "@angular/router";
import {routeConsts} from "../../../shared/routes";
import {Dungeon} from "../../../shared/interfaces/dungeon";
import {Game} from "../../../shared/interfaces/game";

@Component({
  selector: 'hd-character-creator',
  templateUrl: './character-creator.component.html',
  styleUrls: ['./character-creator.component.scss']
})
export class CharacterCreatorComponent implements OnInit {

  constructor(private changeDetectorRefs: ChangeDetectorRef,
              private dialog: MatDialog,
              private dungeonService: DungeonService,
              private route: ActivatedRoute,
              private router: Router){ }

  charForm: FormGroup

  gameID: number
  userID: number
  game : Game

  races: CharacterRace[]
  classes: CharacterClass[]

  ngOnInit(): void {
    //this.testFill();
    this.charForm = new FormGroup({
      charName: new FormControl('', Validators.required),
      race: new FormControl('', Validators.required),
      class: new FormControl('', Validators.required)
    })
    this.userID = +sessionStorage.getItem("userID");
    this.gameID = +this.route.snapshot.paramMap.get('gameID');
    this.dungeonService.searchDungeon(this.gameID).toPromise()
      .then(g => {
        this.game = g;
        this.races = this.game.races;
        this.classes = this.game.classes;
      });

  }

  testFill() : void {
    this.gameID = 1;
    this.userID= 1;
    this.races = []
    this.classes = []
    this.races.push({
      id: 1,
      name: "Mensch",
      description: null,
      attributes: null
    })
    this.races.push({
      id: 2,
      name: "Ork",
      description: null,
      attributes: null
    })
    this.classes.push({
      id: 1,
      name: "Krieger",
      description: null,
      attributes: null
    })
    this.classes.push({
      id: 1,
      name: "Zauberer",
      description: null,
      attributes: null
    })
    this.changeDetectorRefs.detectChanges();
  }

  navigateBack() : void {
    this.router.navigate([routeConsts.dungeonOverViewNav+this.gameID]);
  }

  createChar() : void {
    const charValues = this.charForm.value;
    const newChar : Character = {
      id: 0,
      name: charValues.charName,
      active : false,
      weapon : null,
      armour : null,
      accessoire : null,
      characterRace : charValues.race,
      characterClass : charValues.class,
      inventory : [],
      attributes : {
        attributes: []
      },
      location: null
    };
    console.log(charValues.race)
    console.log(charValues.class)
    this.dungeonService.createCharacter(this.userID, this.gameID, newChar).toPromise()
      .then(result => {
        if(result) {
          this.navigateBack();
        } else {
          this.dialog.open(InfoDialogComponent, {
            data: 'Da hat etwas nicht geklappt. Versuche es bitte erneut!'
          })
        }
      })
      .catch((e) => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Kritischer Fehler. Versuche es bitte erneut!'
        })
        console.log(e)
      });


  }

}
