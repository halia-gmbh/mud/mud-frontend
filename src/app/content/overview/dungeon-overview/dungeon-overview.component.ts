import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {DungeonStatusEnum, Game} from "../../../shared/interfaces/game";
import {ActivatedRoute, Router} from "@angular/router";
import {Character} from "../../../shared/interfaces/character/character";
import {routeConsts} from "../../../shared/routes";
import {DungeonService} from "../dungeon.service";
import {MatDialog} from "@angular/material/dialog";
import {InfoDialogComponent} from "../../../tools/dialogs/info-dialog/info-dialog.component";

@Component({
  selector: 'hd-dungeon-overview',
  templateUrl: './dungeon-overview.component.html',
  styleUrls: ['./dungeon-overview.component.scss']
})
export class DungeonOverviewComponent implements OnInit {

  constructor(private changeDetectorRefs: ChangeDetectorRef, private route: ActivatedRoute,
              private router: Router, private dungeonService: DungeonService,
              private dialog: MatDialog) { }
  gameID: number
  userID: number
  game: Game = {
    id: null,
    name: '',
    builder: 0,
    dungeonMaster: 0,
    maxActivePlayers: 0,
    dungeonStatus: DungeonStatusEnum.WIP,
    classes: [],
    races: [],
    running: false,
    dungeon: {
      startRoom: null,
      itemTemplates: [],
      npcTemplates: [],
      eventTemplates: [],
      rooms: [],
      activeCharacters: []
    },
    password: null
  };
  builder: number
  master: number
  name: string
  maxPlayers: number
  array: string[]
  characters: Character[]
  displayedColumns = ['name', 'race', 'class', 'play']

  ngOnInit(): void {
    this.userID = +sessionStorage.getItem("userID");
    this.gameID = +this.route.snapshot.paramMap.get('gameID');

    this.dungeonService.getCharactersOfDungeon(this.gameID, this.userID)
      .toPromise().then((ob: Character[]) => {
        this.characters = ob;
      });

    this.dungeonService.searchDungeon(this.gameID)
      .toPromise().then(game => {
        this.game = game;
        this.name = this.game.name;
        this.builder = this.game.builder;
        this.master = this.game.dungeonMaster;
        this.maxPlayers = this.game.maxActivePlayers;
      });

  }

  play(row: number) : void {
    const chosenChar = this.characters[row];
    this.dungeonService.joinDungeon(this.gameID, this.userID, chosenChar.id)
      .toPromise().then(game => {
        if(game != null) {
          sessionStorage.setItem('otherID', this.gameID.toString())
          this.router.navigate([routeConsts.dungeonPlayerNav + chosenChar.id + '/' + this.gameID]);
        }
        else
          this.dialog.open(InfoDialogComponent, {
            data: 'Kritischer Fehler! Bitte kontaktieren Sie einen Administrator!'})
      }).catch(() => {
        this.dialog.open(InfoDialogComponent, {
          data: 'Kritischer Fehler! Bitte kontaktieren Sie einen Administrator!'
        })
    })
  }
  back() : void {
    this.router.navigate([routeConsts.overviewNav]);
  }
  toCharacterCreator() : void {
    sessionStorage.setItem('otherID', this.gameID.toString())
    this.router.navigate([routeConsts.characterCreatorNav+this.gameID]);
  }

  deleteCharacter(characterID: number) {
    this.dungeonService.deleteCharacter(this.gameID, characterID, this.userID).toPromise()
      .then(character => {
        if (character) {
          this.characters = character
        }
      })
  }



}
