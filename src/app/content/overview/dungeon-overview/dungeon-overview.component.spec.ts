import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DungeonOverviewComponent } from './dungeon-overview.component';

describe('DungeonOverviewComponent', () => {
  let component: DungeonOverviewComponent;
  let fixture: ComponentFixture<DungeonOverviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DungeonOverviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DungeonOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
