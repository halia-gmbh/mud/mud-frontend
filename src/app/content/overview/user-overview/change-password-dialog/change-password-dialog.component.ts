import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Form, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'hd-change-password-dialog',
  templateUrl: './change-password-dialog.component.html',
  styleUrls: ['./change-password-dialog.component.scss']
})
export class ChangePasswordDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ChangePasswordDialogComponent>) {}

  resultString: string;

  ngOnInit(): void {
  }

  setPublic() : void {
    console.log("PUBLIC")
    this.dialogRef.close(null);
  }

  setPrivate() : void {
    console.log(this.resultString)
    if(this.resultString != "" && typeof this.resultString !== 'undefined') {
      console.log("PRIVATE")
      this.dialogRef.close(this.resultString);
    }
  }

  onNoClick(): void {
    console.log("CLOSE")
    this.dialogRef.close();
  }



}
