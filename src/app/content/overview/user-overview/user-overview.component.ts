import {Component, OnInit} from '@angular/core';
import {DungeonService} from '../dungeon.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {routeConsts} from '../../../shared/routes';
import {Game} from '../../../shared/interfaces/game';
import {ErrorDialogComponent} from '../../../tools/dialogs/error-dialog/error-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {environment} from '../../../../environments/environment';
import {Stomp} from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import {InfoDialogComponent} from '../../../tools/dialogs/info-dialog/info-dialog.component';
import {DungeonMasterService} from "../../game/dungeon-master.service";
import {ChangePasswordDialogComponent} from "./change-password-dialog/change-password-dialog.component";
import {CheckPasswordDialogComponent} from "./check-password-dialog/check-password-dialog.component";

@Component({
  selector: 'hd-user-overview',
  templateUrl: './user-overview.component.html',
  styleUrls: ['./user-overview.component.scss']
})
export class UserOverviewComponent implements OnInit {

  dungeonForm: FormGroup;
  searchForm: FormGroup;
  userGamesNamesOwn: string[] = [];
  userGamesNamesActive: string[] = [];
  userGamesNamesInactive: string[] = [];
  userGameNamesPublic: string[] = [];
  userID: number;
  private stompClient;


  allActiveGames: Game[] = [];
  allInactiveGames: Game[] = [];
  allUserGames: Game[] = [];
  allPublicGames: Game[] = [];


  constructor(private dungeonService: DungeonService, private router: Router, private dialog: MatDialog,
              private masterService: DungeonMasterService) {
  }

  ngOnInit(): void {
    this.userID = +sessionStorage.getItem('userID');
    this.dungeonForm = new FormGroup({
      dungeonName: new FormControl('', Validators.required),
      maxPlayers: new FormControl('', Validators.required),

    });
    this.searchForm = new FormGroup({
      dungeonId: new FormControl('', Validators.required)
    });

    this.refreshAllDungeons();
    this.connect()
  }

  refreshAllDungeons() {
    this.dungeonService.getAllActiveDungeons(this.userID)
      .toPromise()
      .then((userDungeons: Game[]) => {
        this.allActiveGames = userDungeons;
        if (userDungeons) {
          userDungeons.forEach(g => {
            this.userGamesNamesActive.push(g.name);
          });
        }
      });
    this.dungeonService.getAllInactiveDungeons(this.userID)
      .toPromise()
      .then((userDungeons: Game[]) => {
        this.allInactiveGames = userDungeons;
        if (userDungeons) {
          userDungeons.forEach(g => {
            this.userGamesNamesInactive.push(g.name);
          });
        }
      });
    this.dungeonService.getAllUserDungeons(this.userID)
      .toPromise()
      .then((userDungeons: Game[]) => {
        this.allUserGames = userDungeons;
        if (userDungeons) {
          userDungeons.forEach(g => {
            this.userGamesNamesOwn.push(g.name);
          });
        }
      });
    this.dungeonService.getAllPublicDungeons()
      .toPromise()
      .then((dungeon: Game[]) => {
        this.allPublicGames = dungeon;
        if (dungeon) {
          dungeon.forEach(g => {
            this.userGameNamesPublic.push(g.name);
          });
        }
      });
  }

  connect() {
    const socket = new SockJS(`${environment.baseUrl}/ws-chat`);
    this.stompClient = Stomp.over(socket);

    const _this = this;
    this.stompClient.connect({}, function(frame) {
      console.log('Connected: ' + frame);
      _this.stompClient.subscribe(`/topic/admin/`, function(message) {
        _this.dialog.open(InfoDialogComponent, {
          data: message.body
        })
      });
      _this.stompClient.subscribe(`/topic/overviewRefresh/` + _this.userID, function () {
        console.log("refreshingOverview");
        _this.refreshAllDungeons();
      })
    });
  }

  createGame() {
    const dungeonValues = this.dungeonForm.value;
    if (dungeonValues.maxPlayers > 0) {
      this.dungeonService.createGame(dungeonValues.dungeonName, dungeonValues.maxPlayers, +sessionStorage.getItem('userID'))
        .toPromise()
        .then(gameID => {
          sessionStorage.setItem('otherID', gameID.toString())
          this.router.navigate([routeConsts.builderNavGameID + gameID]);
        });
    } else {
      this.dialog.open(ErrorDialogComponent, {
        data: 'Die Spieleranzahl muss positiv sein'
      });
    }

  }

  search() {
    const gameID = this.searchForm.value.dungeonId;

    this.dungeonService.searchDungeon(gameID).toPromise().then(game => {
      console.log(game)
      if (game.running == true)
        this.allActiveGames.push(game)
      else this.allInactiveGames.push(game)
    })
  }



  alterGame(game: Game) {
    let res: Game;
    res = game;
    this.dungeonService.alterGame(res.id)
      .toPromise()
      .then(gameID => {
        sessionStorage.setItem('otherID', gameID.toString())
        this.router.navigate([routeConsts.builderNavGameID + gameID]);
      });
  }

  goToDungeonOverview(game: Game) {
    let res: Game;
    res = game;
    sessionStorage.setItem('otherID', game.id.toString())
    this.router.navigate([routeConsts.dungeonOverViewNav + res.id]);
  }

  startDungeon(gameID: number) {
    this.dungeonService.startGame(gameID, this.userID)
      .toPromise()
      .then(ans => {
        if (ans) {
          sessionStorage.setItem('otherID', gameID.toString())
          this.router.navigate([routeConsts.masterNavGameID + gameID]);
        }
      });
  }

  rollBackDungeon(gameID: number) {
    this.dungeonService.rollBackGame(gameID).toPromise().then(ans => {
      if (ans) {
        this.refreshAllDungeons()
      }
    })
  }

  endGame(game : Game) {
    this.masterService.endDungeon(game.id).toPromise().then(ans => {
      if (ans) {
        this.refreshAllDungeons();
      }
    });
  }

  changePasswordDialog(game : Game) {
    const dialogRef = this.dialog.open(ChangePasswordDialogComponent, {
      width: '250px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if(typeof result !== 'undefined'){
        console.log(result)
        if(result == null) {
          this.changePassword("", game);
          game.password = null;
        }else{
          this.changePassword(result, game);
          game.password = result;
        }
      }
    });
  }

  getGamePasswordDialog(game: Game) {
    this.dungeonService.getGamePassword(game.id).toPromise().then(result => {
      if(result == true) {
        this.goToDungeonOverview(game);
      }else{
        this.checkPasswordDialog(game);
      }
    });
  }

  checkPasswordDialog(game : Game) {
    let foundDungeon : Game = null;
    for(let i = 0; i<this.allInactiveGames.length; i++) {
      if(game.id == this.allInactiveGames[i].id) {
        foundDungeon = this.allInactiveGames[i];
        break;
      }
    }
    if(foundDungeon == null) {
      for(let i = 0; i<this.allActiveGames.length; i++) {
        if(game.id == this.allActiveGames[i].id) {
          foundDungeon = this.allActiveGames[i];
          break;
        }
      }
    }
    if(foundDungeon == null) {
      const dialogRef = this.dialog.open(CheckPasswordDialogComponent, {
        width: '250px'
      });
      dialogRef.afterClosed().subscribe(result => {
        this.dungeonService.checkPassword(game.id, result).toPromise().then(result => {
          if(result == true) {
            this.goToDungeonOverview(game)
          }
        })
      })
    }else{
      this.goToDungeonOverview(game)
    }
  }

  changePassword(password: string, game: Game) {
    this.dungeonService.setPassword(game.id, password).toPromise().then();
  }

}
