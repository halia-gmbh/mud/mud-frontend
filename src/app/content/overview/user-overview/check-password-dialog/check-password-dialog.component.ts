import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'hd-check-password-dialog',
  templateUrl: './check-password-dialog.component.html',
  styleUrls: ['./check-password-dialog.component.scss']
})
export class CheckPasswordDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<CheckPasswordDialogComponent>) {}

  resultString: string;

  ngOnInit(): void {
  }



  onNoClick(): void {
    this.dialogRef.close();
  }


}
