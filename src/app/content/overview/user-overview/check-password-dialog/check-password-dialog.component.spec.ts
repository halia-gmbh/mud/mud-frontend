import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckPasswordDialogComponent } from './check-password-dialog.component';

describe('CheckPasswordDialogComponent', () => {
  let component: CheckPasswordDialogComponent;
  let fixture: ComponentFixture<CheckPasswordDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckPasswordDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckPasswordDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
