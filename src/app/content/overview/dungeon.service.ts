import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Game} from '../../shared/interfaces/game';
import {environment} from '../../../environments/environment';
import {api} from '../../shared/api';
import {Character} from "../../shared/interfaces/character/character";
import { map } from 'rxjs/operators';
import {Message} from '../../shared/interfaces/message';

@Injectable({
  providedIn: 'root'
})
export class DungeonService {

  constructor(private httpClient: HttpClient) { }

  getAllActiveDungeons(accountID: number){
    return this.httpClient.get<Game[]>(`${environment.baseUrl}${api.allActiveDungeons_UserID}${accountID}`)
  }
  getAllInactiveDungeons(accountID: number){
    return this.httpClient.get<Game[]>(`${environment.baseUrl}${api.allInactiveDungeons_UserID}${accountID}`)
  }
  getAllUserDungeons(accountID: number){
    return this.httpClient.get<Game[]>(`${environment.baseUrl}${api.ownBuiltDungeons_UserID}${accountID}`)
  }
  getAllPublicDungeons() {
    return this.httpClient.post<Game[]>(`${environment.baseUrl}${api.allPublicGames}`, "")
  }
  getGamePassword(gameID: number) {
    return this.httpClient.post<boolean>(`${environment.baseUrl}${api.getGamePassword}${gameID}/`, "");
  }

  checkPassword(gameID: number, password: string) {
    return this.httpClient.post<boolean>(`${environment.baseUrl}${api.checkPassword}${gameID}/${password}`, "");
  }

  startGame(gameID: number, userID: number) {
    return this.httpClient.post<boolean>(`${environment.baseUrl}${api.startDungeon_GameID_UserID}${gameID}/${userID}`, '')
  }

  setPassword(gameID: number, password: string) {
    console.log(password)
    return this.httpClient.post<void>(`${environment.baseUrl}${api.setPassword_gameID}${gameID}`, password)
  }

  rollBackGame(gameID: number) {
    return this.httpClient.post<boolean>(`${environment.baseUrl}${api.rollBackDungeon_GameID}${gameID}`, '')
  }

  createGame(name: string, maxPlayers: number, builderID: number) {
    return this.httpClient.post<number>(`${environment.baseUrl}${api.createDungeon_Name_MaxPlayers_DungeonBuilderID}${name}/${maxPlayers}/${builderID}`, '')
  }

  getGame(gameID: number) {
    return this.httpClient.get<any>(`${environment.baseUrl}${api.getFullGame_GameID}${gameID}`)
      .pipe(
        map(game => {
          const g: Game = {
            ...game,
            dungeon: {
              ...game.dungeon,
              rooms: []
            }
          }
          for (let t in game.dungeon.rooms)
            g.dungeon.rooms.push(t)
          return g
        })
      )
  }

  createCharacter(accountID: number, gameID: number, character: Character) {
    return this.httpClient.post<boolean>(`${environment.baseUrl}${api.createCharacter_UserID_GameID}${accountID}/${gameID}`, character);
  }
  deleteCharacter(gameID: number, characterID: number, userID: number) {
    return this.httpClient.post<Character[]>(`${environment.baseUrl}${api.deleteCharacter_GameID_CharacterID_UserID}${gameID}/${characterID}/${userID}`, '');
  }

  searchDungeon(gameID: number) {
    return this.httpClient.get<Game>(`${environment.baseUrl}${api.searchDungeon_GameID}${gameID}`)
  }

  getCharactersOfDungeon(gameID: number, accountID: number) {
    return this.httpClient.get<Character[]>(`${environment.baseUrl}${api.getCharactersOfDungeon_GameID_AccountID}${accountID}/${gameID}`);
  }

  joinDungeon(gameID: number, userID: number, characterID: number) {
    return this.httpClient.post<Game>(`${environment.baseUrl}${api.joinDungeon_GameID_UserID_CharacterID}${gameID}/${userID}/${characterID}`, '')
  }

  alterGame(gameID: number) {
    return this.httpClient.post<number>(`${environment.baseUrl}${api.alterGame_GameID}${gameID}`, '')
  }

  chat(msg: Message) {
    return this.httpClient.post<void>(`${environment.baseUrl}${api.chat}`, msg);
  }
}
