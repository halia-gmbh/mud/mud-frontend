import { Component, OnInit} from '@angular/core';
import {AuthService} from "../../auth/auth.service";
import {routeConsts} from '../../shared/routes';
import {Router} from '@angular/router';
import {ErrorDialogComponent} from "../../tools/dialogs/error-dialog/error-dialog.component";
import {MatDialog} from "@angular/material/dialog";


@Component({
  selector: 'hd-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {


  overviewRoute = routeConsts.overviewNav
  isAuth = true

  constructor(private authService: AuthService, private router: Router, private dialog: MatDialog) { }

  ngOnInit(): void {
  }

  logout() {
    const userID = sessionStorage.getItem('userID')
    if (userID)
      this.authService.logout(userID).then(e => {
        this.router.navigate([routeConsts.login]);
      })


  }

  settings() {
    const userID = sessionStorage.getItem('userID')
    if (userID)
      this.router.navigate([routeConsts.settingsNav])
  }

}
