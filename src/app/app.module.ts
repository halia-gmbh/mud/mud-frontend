import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FlexLayoutModule} from '@angular/flex-layout';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthHttpInterceptorService} from './auth/auth-http-interceptor.service';


// MATERIAL
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTableModule} from '@angular/material/table';
import {MatExpansionModule} from "@angular/material/expansion";
import {MatMenuModule} from '@angular/material/menu';
import {MatDividerModule} from '@angular/material/divider';
import {MatRadioModule} from '@angular/material/radio';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';


// COMPONENTS
import { AppComponent } from './app.component';
import { HeaderComponent } from './navigation/header/header.component';
import { SidebarComponent } from './navigation/sidebar/sidebar.component';
import { AccountSettingsComponent } from './content/account/account-settings/account-settings.component';
import { DungeonOverviewComponent } from './content/overview/dungeon-overview/dungeon-overview.component';
import { CharacterCreatorComponent } from './content/overview/character-creator/character-creator.component';
import { UserOverviewComponent } from './content/overview/user-overview/user-overview.component';
import { DungeonPlayerComponent } from './content/game/dungeon-player/dungeon-player.component';
import { DungeonMasterComponent } from './content/game/dungeon-master/dungeon-master.component';
import { DungeonBuilderComponent } from './content/builder/dungeon-builder/dungeon-builder.component';
import { LoginComponent } from './content/login/login.component';
import { ErrorDialogComponent } from './tools/dialogs/error-dialog/error-dialog.component';
import { InfoDialogComponent } from './tools/dialogs/info-dialog/info-dialog.component';
import { ResetPasswordComponent } from './content/login/reset-password/reset-password.component';
import { ConfirmAccountComponent } from './content/login/confirm-account/confirm-account.component';
import {MatSelectModule} from '@angular/material/select';
import { AdminLoginComponent } from './content/admin/admin-login/admin-login.component';
import { AdminOverviewComponent } from './content/admin/admin-overview/admin-overview.component';
import { ChangePasswordDialogComponent } from './content/overview/user-overview/change-password-dialog/change-password-dialog.component';
import { CheckPasswordDialogComponent } from './content/overview/user-overview/check-password-dialog/check-password-dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    AccountSettingsComponent,
    DungeonOverviewComponent,
    CharacterCreatorComponent,
    UserOverviewComponent,
    DungeonPlayerComponent,
    DungeonMasterComponent,
    DungeonBuilderComponent,
    LoginComponent,
    ErrorDialogComponent,
    InfoDialogComponent,
    ResetPasswordComponent,
    ConfirmAccountComponent,
    AdminLoginComponent,
    AdminOverviewComponent,
    ChangePasswordDialogComponent,
    CheckPasswordDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FlexLayoutModule,

    // MATERIAL
    HttpClientModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatSnackBarModule,
    MatCardModule,
    MatToolbarModule,
    MatMenuModule,
    MatTabsModule,
    MatTableModule,
    MatExpansionModule,
    MatDividerModule,
    FormsModule,
    MatSelectModule,
    MatRadioModule,
    MatSlideToggleModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS, useClass: AuthHttpInterceptorService, multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
