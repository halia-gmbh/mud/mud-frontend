import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './content/login/login.component';
import {UserOverviewComponent} from './content/overview/user-overview/user-overview.component';
import {ResetPasswordComponent} from './content/login/reset-password/reset-password.component';
import { routeConsts } from './shared/routes';
import {AccountSettingsComponent} from './content/account/account-settings/account-settings.component';
import {AuthGuard} from './auth/auth.guard';
import {DungeonBuilderComponent} from './content/builder/dungeon-builder/dungeon-builder.component';
import {ConfirmAccountComponent} from "./content/login/confirm-account/confirm-account.component";
import {CharacterCreatorComponent} from "./content/overview/character-creator/character-creator.component";
import {DungeonOverviewComponent} from "./content/overview/dungeon-overview/dungeon-overview.component";
import {DungeonPlayerComponent} from "./content/game/dungeon-player/dungeon-player.component";
import {DungeonMasterComponent} from './content/game/dungeon-master/dungeon-master.component';
import {AdminLoginComponent} from "./content/admin/admin-login/admin-login.component";
import {AdminOverviewComponent} from "./content/admin/admin-overview/admin-overview.component";

const routes: Routes = [
  { path: routeConsts.start, redirectTo: routeConsts.login, pathMatch: 'full' },
  { path: routeConsts.login, component: LoginComponent },
  { path: routeConsts.resetPassword, component: ResetPasswordComponent },
  { path: routeConsts.confirmAccount, component: ConfirmAccountComponent},
  { path: routeConsts.overview, component: UserOverviewComponent, canActivate: [AuthGuard] },
  { path: routeConsts.settings, component: AccountSettingsComponent, canActivate: [AuthGuard] },
  { path: routeConsts.builder, component: DungeonBuilderComponent, canActivate: [AuthGuard] },
  { path: routeConsts.master, component: DungeonMasterComponent , canActivate: [AuthGuard] },
  { path: routeConsts.characterCreator, component: CharacterCreatorComponent, canActivate: [AuthGuard] },
  { path: routeConsts.dungeonOverview, component: DungeonOverviewComponent, canActivate: [AuthGuard] },
  { path: routeConsts.characterCreator, component: CharacterCreatorComponent, canActivate: [AuthGuard] },
  { path: routeConsts.dungeonPlayer, component: DungeonPlayerComponent, canActivate: [AuthGuard] },
  { path: routeConsts.adminLogin, component: AdminLoginComponent},
  { path: routeConsts.adminOverview, component: AdminOverviewComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
