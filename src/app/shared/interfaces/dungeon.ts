import {Room} from './room/room';
import {Item} from './room/item';
import {NPC} from './room/npc';
import {RoomEvent} from './room/room-event';
import {Character} from './character/character';

export interface Dungeon {
  startRoom: Room
  itemTemplates: Item[]
  npcTemplates: NPC[]
  eventTemplates: RoomEvent[]
  rooms: any[]
  activeCharacters: Character[]
}
