export interface Account {
  id: number
  email: string
  token: string
  unlocked: boolean
}
