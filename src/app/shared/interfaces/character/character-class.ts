import {AttributeSet} from './attribute-set';

export interface CharacterClass {
  id: number
  name: string
  description: string
  attributes: AttributeSet
}

