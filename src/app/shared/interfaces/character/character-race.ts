import {AttributeSet} from './attribute-set';

export interface CharacterRace {
  id: number
  name: string
  description: string
  attributes: AttributeSet
}
