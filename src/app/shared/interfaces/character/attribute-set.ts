export interface AttributeSet {
  attributes: Attribute[]
}

export interface Attribute {
  name: string
  value: number
}
