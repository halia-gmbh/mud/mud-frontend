import {Item} from '../room/item';
import {CharacterRace} from './character-race';
import {CharacterClass} from './character-class';
import {AttributeSet} from './attribute-set';
import {Room} from '../room/room';

export interface Character {
  id: number,
  name: string,
  active: boolean,
  attributes: AttributeSet,
  weapon: Item,
  armour: Item,
  accessoire: Item,
  inventory: Item[],
  characterRace: CharacterRace,
  characterClass: CharacterClass,
  location: Room
}
