import {Game} from './game';

export interface DungeonOverview {
  games: Game[]
}
