import {CharacterClass} from './character/character-class';
import {CharacterRace} from './character/character-race';
import {Dungeon} from './dungeon';

export interface Game {
  id: number
  name: string
  builder: number
  dungeonMaster: number
  maxActivePlayers: number
  dungeonStatus: DungeonStatusEnum
  classes: CharacterClass[]
  races: CharacterRace[]
  dungeon: Dungeon
  running: boolean
  password: string
}

export enum DungeonStatusEnum {
  WIP,
  PUBLIC
}
