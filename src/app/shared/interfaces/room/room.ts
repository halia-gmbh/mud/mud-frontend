import {Item} from './item';
import {NPC} from './npc';
import {RoomEvent} from './room-event';
import {Direction} from './direction';

export interface Room {
  id: number
  name: string
  description: string
  items: Item[]
  npcs: NPC[]
  roomEvents: RoomEvent[]
  presentCharacters: number[]
}

export interface MatrixRoom {
  room: Room,
  north: boolean
  east: boolean
  south: boolean
  west: boolean
  startRoom: boolean
  deletePath?: boolean
  pathChangeDirection?: Direction
}
