export interface NPC {
  id: number,
  name: string
  actions: NPCAction[]
}

export interface NPCAction {
  name: string,
  response: string
}
