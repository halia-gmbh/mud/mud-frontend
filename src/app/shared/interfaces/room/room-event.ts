import {Item} from './item';

export interface RoomEvent {
  id: number,
  name: string,
  textResponse: string
  itemResponse: Item
}
