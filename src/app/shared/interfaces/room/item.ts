import {AttributeSet} from '../character/attribute-set';

export interface Item {
  id: number,
  name: string,
  weight: number,
  attributes: AttributeSet,
  itemType: ItemTypeEnum
}

export enum ItemTypeEnum {
  CONSUMABLE,
  ARMOUR,
  WEAPON,
  ACCESSOIRE
}
