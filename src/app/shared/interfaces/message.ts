export interface Message {
  senderName: string,
  receiverName: string,
  content: string,
  type: MessageType,
  roomID: number,
  gameID: number
}

export enum MessageType {
  ChatWhisper,
  ChatTalk,
  DmToPlayer,
  PlayerToDm,
  DmTalk,
  DmShout,
  Admin,
  CubeEvent

}
