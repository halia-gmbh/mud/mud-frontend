export const api = {
  // AUTH
  login: '/api/account/login',
  register: '/api/account/register',
  resetPassword: '/api/account/reset',
  confirmAccount: '/api/account/confirm',
  logout: '/api/account/logout/',
  checkAuthState: '/api/check',
  newPassword_UserID_Password: '/api/changePassword/',
  newEmail_UserID_Email:'/api/changeEmail/',

  // OVERVIEW
  createCharacter_UserID_GameID: '/overview/createCharacter/',
  deleteCharacter_GameID_CharacterID_UserID: '/overview/deleteCharacter/',
  searchDungeon_GameID: '/overview/searchDungeon/',
  getCharactersOfDungeon_GameID_AccountID: '/overview/getCharacters/',
  joinDungeon_GameID_UserID_CharacterID: '/overview/joinDungeon/',
  allActiveDungeons_UserID: '/overview/getActiveGames/user/',
  ownBuiltDungeons_UserID: '/overview/getOwnBuiltGames/user/',
  allInactiveDungeons_UserID: '/overview/getInactiveGames/user/',
  allPublicGames: '/overview/getPublicDungeons/',
  setPassword_gameID: '/overview/setPassword/',
  getGamePassword: '/overview/getGamePassword/',
  checkPassword: '/overview/checkPassword/',

  // BUILDER
  saveDungeon_GameID: '/dungeonBuilder/saveDungeon/game/',
  alterGame_GameID: '/dungeonBuilder/alterDungeon/game/',
  publishDungeon_GameID: '/dungeonBuilder/publishDungeon/game/',
  deleteDungeon_GameID: '/overview/deleteDungeon/',

  createRoom_GameID_Name_Description: '/dungeonBuilder/addRoom/game/roomName/roomDescription/',
  connectRooms_GameID_From_To_Direction: '/dungeonBuilder/addPathway/game/from/to/direction/',
  removePathway_GameID_From_To: '/dungeonBuilder/removePathway/game/from/to/',
  deleteRoom_GameID_RoomID: '/dungeonBuilder/removeRoom/game/room/',
  getMatrix_GameID: '/dungeonBuilder/getMatrix/game/',
  editRoom_GameID_RoomID_Name_Description: '/dungeonBuilder/editName/game/room/name/description/',

  getFullGame_GameID: '/dungeonBuilder/getGame/',
  addCharacterClass_GameID: '/dungeonBuilder/addCharacterClass/game/characterClass/',
  addCharacterRace_GameID: '/dungeonBuilder/addCharacterRace/game/race/',
  removeCharacterClass_GameID_ClassID: '/dungeonBuilder/removeCharacterClass/game/classId/',
  removeCharacterRace_GameID_Race_ID: '/dungeonBuilder/removeCharacterRace/game/raceId/',
  addItemTemplate_GameID: '/dungeonBuilder/addItemTemplate/game/',
  addRoomEventTemplate_GameID: '/dungeonBuilder/addRoomEventTemplate/game/',
  addNpcTemplate_GameID: '/dungeonBuilder/createNPCTemplate/game/',
  addItemToRoom_GameID_RoomID: '/dungeonBuilder/addItem/game/room/',
  addRoomEventToRoom_GameID_RoomID: '/dungeonBuilder/addRoomEvent/game/room/',
  addNpcToRoom_GameID_RoomID: '/dungeonBuilder/addNPC/game/room/',

  removeItemTemplate_GameID_ItemID: '/dungeonBuilder/removeItemTemplate/game/item/',
  removeRoomEventTemplate_GameID_RoomEventID: '/dungeonBuilder/removeEventTemplate/game/event/',
  removeNpcTemplate_GameID_NpcID: '/dungeonBuilder/removeNPCTemplate/game/npc/',
  removeItemFromRoom_GameID_RoomID_ItemID: '/dungeonBuilder/removeItem/game/room/item/',
  removeRoomEventFromRoom_GameID_RoomID_RoomEventID: '/dungeonBuilder/removeEvent/game/room/event/',
  removeNpcFromRoom_GameID_RoomID_NpcID: '/dungeonBuilder/removeNPC/game/room/npc/',


  // DUNGEON
  createDungeon_Name_MaxPlayers_DungeonBuilderID: '/dungeonBuilder/createGame/name/maxPlayers/builderID/',
  startDungeon_GameID_UserID: '/overview/startDungeon/game/user/',
  rollBackDungeon_GameID: '/overview/rollbackDungeon/',


  // MASTER
  setAttribute_GameID_CharacterID_AttIndex_AttName_Value: '/dungeonMaster/setPlayerAttribute/game/character/index/attribute/value/',
  deleteAttribute_GameID_CharacterID_AttName: '/dungeonMaster/deletePlayerAttribute/game/character/attribute/value/',
  setItem_GameID_CharacterID: '/dungeonMaster/givePlayerItem/game/character/item/',
  deleteItem_GameID_CharacterID_ItemID: '/dungeonMaster/deletePlayerItem/game/character/item/',
  placePlayer_GameID_CharacterID_RoomID: '/dungeonMaster/placeCharacter/game/',
  endGame_GameID: '/dungeonMaster/endGame/game/',
  resetDeathWatch_GameID: '/dungeonMaster/resetDeathWatch/game/',
  setDeathWatch_GameID: '/dungeonMaster/setDeathWatch/game/',
  changeDM_GameID_CharacterID: '/dungeonMaster/changeDungeonMaster/game/character/',

  // PLAYER
  pickUpItem_GameID_CharacterID_ItemName: '/dungeonPlayer/pickUpItem/game/character/item/',
  useItem_GameID_CharacterID_ItemID: '/dungeonPlayer/useItem/game/character/item/',
  unEquipItem_GameID_CharacterID_ItemID: '/dungeonPlayer/unequip/game/character/item/',
  moveToRoom_GameID_CharacterID_Direction: '/dungeonPlayer/move/game/character/direction/',
  discardItem_GameID_CharacterID_ItemID: '/dungeonPlayer/discardItem/game/character/item/',
  interactWithRoomEvent_GameID_CharacterID_EventName: '/dungeonPlayer/interactRoomevent/game/character/event/',
  getCharactersInRoom_GameID_RoomID: '/dungeonPlayer/getCharactersInRoom/game/character/direction/',
  quitGame: '/dungeonPlayer/quitGame/game/character/',
  setPlayerDeathWatch_GameID_CharacterID: '/dungeonPlayer/setDeathWatch/',
  resetPlayerDeathWatch_CharacterID: '/dungeonPlayer/resetDeathWatch/',

  // CHAT
  chat: '/api/send',

  // ADMIN
  adminLogin: '/api/admin/login',
  adminGetAllAccouts: '/api/admin/getAllAccounts',
  adminGetAllPlayingAccouts: '/api/admin/getAllPlayingAccounts',
  adminDeleteAccount_AccountID: '/api/delete/',
  adminVerifyAccount_AccountID: '/api/admin/verify/',
  adminMessage: '/api/adminMessage',

}
