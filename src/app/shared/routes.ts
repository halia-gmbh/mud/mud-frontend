export const routeConsts = {
  start: "",

  login: "login",
  loginNav: "/login",

  overview: "overview",
  overviewNav: "/overview",

  dungeonOverview: "overview/dungeon/:gameID",
  dungeonOverViewNav : "/overview/dungeon/",

  dungeonPlayer: "dungeon-player/:playerID/:gameID",
  dungeonPlayerNav: "/dungeon-player/",

  characterCreator: "overview/characterCreator/:gameID",
  characterCreatorNav: "/overview/characterCreator/",

  resetPassword: "reset",
  resetPasswordNav: "/reset",

  settings: "settings",
  settingsNav: "/settings",

  confirmAccount: "confirm",
  confirmAccountNav: "/confirm",

  builder: "dungeon-builder/:gameID",
  builderNavGameID: "/dungeon-builder/",

  master: "dungeon-master/:gameID",
  masterNavGameID: "/dungeon-master/",

  adminLogin: "admin",
  adminLoginNav: "/admin",

  adminOverview: "admin-overview",
  adminOverviewNav: "/admin-overview",
}
